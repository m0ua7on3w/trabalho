//
// Created by M0ua7on3w on 20/01/2017.
//

#include <cmath>
#include "Objetivos.h"


Objetivos::Objetivos(int nivel, TiposIA ia, TipoSecundario ts) {
    _tipoIA = ia;
    _nivel = nivel;
    _turno = 1;
    _fase = 1;
    _tipoS = ts;
    switch (_tipoIA){
        case AGRESSIVO:
            MUDANCA_FASE = 100;
            _porCentoRecolhe = 0.6f;
            break;
        case PASSIVO:
            MUDANCA_FASE = 200;
            _porCentoRecolhe = 0.8f;
            break;
    }

    switch (_tipoS){
        case CRIADOR:
            BASE_SERES = 5;
            BASE_QUINTAS = 2;
            BASE_TORRES = 2;
            BASE_NIVEL_TORRES = 2;
            BASE_NIVEL_QUINTAS = 2;
            break;
        case CONSTRUTOR:
            BASE_SERES = 2;
            BASE_QUINTAS = 5;
            BASE_TORRES = 5;
            BASE_NIVEL_TORRES = 3;
            BASE_NIVEL_QUINTAS = 3;
            break;
        case HIBRIDO:
            BASE_SERES = 3;
            BASE_QUINTAS = 3;
            BASE_TORRES = 3;
            BASE_NIVEL_TORRES = 3;
            BASE_NIVEL_QUINTAS = 3;
            break;
    }
    MudarObjetivos();
}

void Objetivos::MudarObjetivos() {
    //Vamos aumentar os objetivos
    _numSeresPorPerfil = FuncaoObjetivo(BASE_SERES);
    _numQuintas = FuncaoObjetivo(BASE_QUINTAS);
    _numTorres = FuncaoObjetivo(BASE_TORRES);
    _nivelTotalQuintas = FuncaoObjetivo(BASE_NIVEL_QUINTAS);
    _nivelTotalTorres = FuncaoObjetivo(BASE_NIVEL_TORRES);

}

void Objetivos::AumentarTurno() {
    _turno++;
    if(_turno%MUDANCA_FASE==0)
        MudarFase();
}

void Objetivos::MudarFase() {
    _fase++;
    _porCentoRecolhe-=0.1f;
    MudarObjetivos();
}

int Objetivos::FuncaoObjetivo(int Base) {
    return (int)_nivel*Base*_fase;
    return (int)pow(_nivel* Base,_fase);
}
