//
// Created by M0ua7on3w on 20/01/2017.
//

#ifndef TRABALHO_OBJETIVOS_H
#define TRABALHO_OBJETIVOS_H


class Objetivos {
    int _turno;                 //Numero de Instantes do jogo

    int _numSeresPorPerfil;     //Objetivo de seres a criar
    int _numQuintas;            //Objetivo de Quintas a criar
    int _numTorres;             //Objetivo de Torres a criar
    int _nivelTotalQuintas;     //Objetivo de Nivel das Quintas
    int _nivelTotalTorres;      //Objetivo de Nivel das Torres

    float _porCentoRecolhe;     //Percentagem de recolhe ( quando ficar inferior a esse valor, a colonia recua)
    int _nivel;                 //Nivel do Objetivo (Dificuldade do ser talvez, quanto maior mais objetivos tera de obter)
    int _fase;                  //Fase atual ( i.e Early, Mid, Late game ) cada fase aumenta o nivel


    //Meta? Turnos? Isso tem de ser mudado na apresentacao
    int MUDANCA_FASE;

    int BASE_SERES;
    int BASE_QUINTAS;
    int BASE_TORRES;
    int BASE_NIVEL_TORRES;
    int BASE_NIVEL_QUINTAS;

    void MudarObjetivos();
    void MudarFase();
    int FuncaoObjetivo(int Base);
public:
    enum TiposIA{
        AGRESSIVO = 0,
        PASSIVO = 1,
    };
    enum TipoSecundario{
        CRIADOR = 0,
        CONSTRUTOR = 1,
        HIBRIDO = 2
    };
    TiposIA _tipoIA;
    TipoSecundario _tipoS;
    void AumentarNivel(){_nivel++;MudarObjetivos();}
    Objetivos(int nivel, TiposIA ia, TipoSecundario ts);
    void AumentarTurno();
    int Seres(){return _numSeresPorPerfil;}
    int Quintas(){return _numQuintas;}
    int Torres(){return _numTorres;}
    int NivelQuintas(){return _nivelTotalQuintas;}
    int NivelTorres(){return _nivelTotalTorres;}
    float PorCentoRecolhe(){return _porCentoRecolhe;}

};


#endif //TRABALHO_OBJETIVOS_H
