//
// Created by M0ua7on3w on 14/12/2016.
//

#ifndef TRABALHO_CONFIGURACAO_H
#define TRABALHO_CONFIGURACAO_H


#include "Input.h"

class Configuracao : public Input{
public:
    Configuracao(Game *gm);
    bool ObterInput(std::stringstream &sstr);


};


#endif //TRABALHO_CONFIGURACAO_H
