//
// Created by andre on 16/01/2017.
//

#include "Mensagem.h"
#include "Consola/Consola.h"
#include "sstream"


void Mensagem::Sucesso(std::string str) {
    Consola::setTextColor(Consola::VERDE);
    std::cout << str;
    Consola::setTextColor(Consola::BRANCO);
}

void Mensagem::Normal(std::ostringstream &oss) {
    std::cout << oss.str();
}

void Mensagem::Erro(std::string str) {
    Consola::setTextColor(Consola::VERMELHO);
    std::cout << str;
    Consola::setTextColor(Consola::BRANCO);

}

std::string Mensagem::IntToString(int number) {
    std::ostringstream oss;
    oss<< number;
    return oss.str();
}

void Mensagem::Info(std::string str) {
    Consola::setTextColor(Consola::AZUL_CLARO);
    std::cout << str;
    Consola::setTextColor(Consola::BRANCO);
}


