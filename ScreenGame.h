//
// Created by M0ua7on3w on 14/12/2016.
//

#ifndef TRABALHO_SCREENGAME_H
#define TRABALHO_SCREENGAME_H


#include "Screen.h"
#include <iostream>
class Mapa;
class ScreenGame : public Screen{
    Mapa *_mapa;
    //Tamanhos em espacos
    int Largura = 51;
    int Altura= 26;

    //Onde comeca a imprimir
    //  ( espacos do mapa, mudado pelo comando foco)
    int startingX=0;
    int startingY=0;

    std::string Espacar(int n);
public:
    ScreenGame(Mapa *mapa);
    void Draw();
    void Clear();
    void MudarFoco(int linha, int coluna);
};


#endif //TRABALHO_SCREENGAME_H
