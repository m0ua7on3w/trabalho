//
// Created by M0ua7on3w on 06/12/2016.
//

#include "Caracteristica.h"

int Caracteristica::get_id() const {
    return _id;
}

const string &Caracteristica::get_nome() const {
    return _nome;
}

int Caracteristica::get_custoMonetario() const {
    return _custoMonetario;
}

int Caracteristica::get_custoForca() const {
    return _custoForca;
}

void Caracteristica::SetSer(Ser *s) {
    _meuSer = s;
}

