//
// Created by M0ua7on3w on 14/12/2016.
//

#include <algorithm>
#include "Configuracao.h"
#include "Comandos/Simulacao/setMoedas.h"
#include "Comandos/Configuracao/addperfil.h"
#include "Comandos/Configuracao/casteloC.h"
#include "Comandos/Configuracao/dim.h"
#include "Comandos/Configuracao/inicio.h"
#include "Comandos/Configuracao/loadFile.h"
#include "Comandos/Configuracao/mkperfil.h"
#include "Comandos/Configuracao/moedas.h"
#include "Comandos/Configuracao/oponentes.h"
#include "Comandos/Configuracao/rmperfil.h"
#include "Comandos/Configuracao/subperfil.h"
#include "Consola/Consola.h"

Configuracao::Configuracao(Game *gm)
{
    TAM = 10;
    _comandos=new Comando*[TAM]{new addperfil(gm),
                                new casteloC(gm),
                                new dim(gm),
                                new inicio(gm),
                                new loadFile(gm),
                                new mkperfil(gm),
                                new moedas(gm),
                                new oponentes(gm),
                                new rmperfil(gm),
                                new subperfil(gm)
    };
}


bool Configuracao::ObterInput(stringstream &sstr) {
    string word;
    sstr >> word;
    bool comandoFound;
    comandoFound = false;

    for (int i = 0; i < TAM; i++) {
        if (_comandos[i]->Nome() == word) {
            if (_comandos[i]->VerificarArgs(sstr)) {
                //ssLista << sstr.str() << endl;
                comandoFound = true;
                _comandos[i]->Aplicar();
                Consola::setTextColor(Consola::VERDE);
                std::cout << "\tComando " << _comandos[i]->Nome() << " configurado.";
                Consola::setTextColor(Consola::BRANCO);
                if (_comandos[i]->MudarJogador())
                    return false;
            }
        }
    }
    if (!comandoFound) {
        Consola::setTextColor(Consola::VERMELHO);
        std::cout << "ERRO! Por favor verifique o comando introduzido!";
        Consola::setTextColor(Consola::BRANCO);

    }
    return true;
}

