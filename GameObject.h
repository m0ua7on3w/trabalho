//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_GAMEOBJECT_H
#define TRABALHO_GAMEOBJECT_H

#include <iostream>
class Ponto;
class Colonia;
class Castelo;
class Mapa;


class GameObject {
protected:
    static int EID;
    std::string _nome;
    int _myID;
    Ponto *_pos;
    int _vida;
    int _vidaMaxima;
    int _idNacao;
    int _defesa = 0;
    Castelo* _casa;
public:
    virtual ~GameObject();

    virtual int ObterIdNacao() const {return _idNacao;}
    virtual Ponto * ObterPosicao() {return _pos;}
    virtual int ObterEID(){return _myID;}
    int getDefesa() const { return _defesa; }
    int getVida() const { return _vida;}
    int getVidaMaxima() const { return _vidaMaxima;}
    std::string virtual ObterNome() {return _nome;}

    virtual void ReceberDano(int dano);
    virtual void Atuar(Mapa *mapa)=0;
    virtual void Mata(Mapa *mapa, Colonia *c);

    virtual void print(std::ostream& where) const=0;

    friend std::ostream &operator<<(std::ostream &os, const GameObject *gameObject);

};


#endif //CASTLEWARZ_GAMEOBJECT_H
