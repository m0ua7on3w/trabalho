//
// Created by M0ua7on3w on 07/12/2016.
//

#include <cstdlib>
#include "Ponto.h"
#include <algorithm>

void Ponto::MudarXY(int x, int y) {
    _x=x;
    _y=y;
}


int Ponto::Distancia(Ponto *p) {
    return std::max(abs(_x-p->_x),abs(_y-p->_y));
}

void Ponto::MeuXY(int &x, int &y) {
    x = _x;
    y = _y;
}

float Ponto::DistanciaHipotenusa(Ponto *p) {
    return sqrt(pow(_x-p->_x,2)+pow(_y-p->_y,2));
}

