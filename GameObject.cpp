//
// Created by M0ua7on3w on 06/12/2016.
//

#include "GameObject.h"

#include "Mapa.h"
#include "Colonia.h"
#include "Ponto.h"
using namespace std;
int GameObject::EID=0;

void GameObject::ReceberDano(int dano) {
    if(dano<0){
        _vida-=dano;
        if(_vida>_vidaMaxima)
            _vida=_vidaMaxima;
    }else{
        _vida-= dano;
        if(_vida<0){
            _vida=0;
        }
    }

}

void GameObject::Mata(Mapa *mapa, Colonia *c) {
    if(this->_vida==0){
        mapa->Remover(this);
        c->Eliminar(this);
    }
}

std::ostream &operator<<(std::ostream &os, const GameObject *gameObject) {
    gameObject->print(os);
    return os;
}

GameObject::~GameObject() {
    delete _pos;
}

