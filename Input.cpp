//
// Created by M0ua7on3w on 11/12/2016.
//

#include "Input.h"
#include "Comando.h"
#include "Comandos/Simulacao/setMoedas.h"
#include "Comandos/Simulacao/ataca.h"
#include "Comandos/Simulacao/nextC.h"
#include "Comandos/Simulacao/mkbuild.h"
#include "Comandos/Simulacao/load.h"
#include "Comandos/Simulacao/listp.h"
#include "Comandos/Simulacao/listallp.h"
#include "Comandos/Simulacao/list.h"
#include "Comandos/Simulacao/foco.h"
#include "Comandos/Simulacao/fim.h"
#include "Comandos/Simulacao/erase.h"
#include "Comandos/Simulacao/build.h"
#include "Comandos/Simulacao/nextn.h"
#include "Comandos/Simulacao/recolhe.h"
#include "Comandos/Simulacao/repair.h"
#include "Comandos/Simulacao/restore.h"
#include "Comandos/Simulacao/save.h"
#include "Comandos/Simulacao/sell.h"
#include "Comandos/Simulacao/serC.h"
#include "Comandos/Simulacao/upgrade.h"
#include "Comandos/Simulacao/zoomout.h"
#include "Consola/Consola.h"
#include <algorithm>


Input::Input(Game *jogo)
{
    TAM = 21;
    _comandos=new Comando*[TAM]{new ataca(jogo),
                                new build(jogo),
                                new erase(jogo),
                                new fim(jogo),
                                new foco(jogo),
                                new list(jogo),
                                new listallp(jogo),
                                new listp(jogo),
                                new load(jogo),
                                new mkbuild(jogo),
                                new nextC(jogo),
                                new nextn(jogo),
                                new recolhe(jogo),
                                new repair(jogo),
                                new restore(jogo),
                                new save(jogo),
                                new sell(jogo),
                                new serC(jogo),
                                new setMoedas(jogo),
                                new upgrade(jogo),
                                new zoomout(jogo)
    };
}

bool Input::ObterInput(std::stringstream &sstr) {
    string line,word;
    stringstream ss(line);
    sstr >> word;

    bool verificar = false;

    for(int i = 0; i < TAM; i++)
    {
        if(_comandos[i]->Nome()==word)
        {
            if(_comandos[i]->VerificarArgs(sstr))
            {
                verificar = true;
                _comandos[i]->Aplicar();
                Consola::setTextColor(Consola::VERDE);
                std::cout << "\tComando " << _comandos[i]->Nome() << " simulado.";
                Consola::setTextColor(Consola::BRANCO);

                return _comandos[i]->MudarJogador();
            }
        }
    }

    if(!verificar)
    {
        Consola::setTextColor(Consola::VERMELHO);
        std::cout << "ERRO! Por favor verifique o comando introduzido!" << endl;
        Consola::setTextColor(Consola::BRANCO);
        return false;
    }
}

Input::Input() {

}

Input::~Input() {
    for(int i = 0; i<TAM; i++){
        delete _comandos[i];
    }
    delete _comandos;
}


