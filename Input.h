//
// Created by M0ua7on3w on 11/12/2016.
//

#ifndef TRABALHO_INPUT_H
#define TRABALHO_INPUT_H


#include <iostream>

class Comando;
class Game;

class Input {
protected:
    int TAM = 1;
    Comando **_comandos;
public:
    Input(Game *jogo);
    Input();

    virtual ~Input();

    virtual bool ObterInput(std::stringstream &sstr);
};


#endif //TRABALHO_INPUT_H
