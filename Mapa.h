//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_MAPA_H
#define TRABALHO_MAPA_H

#include <iostream>
#include <vector>

class GameObject;

class Mapa {
    std::vector<GameObject*> _obj;
    int _linhas, _colunas;
public:
    Mapa(int linhas, int colunas);
    GameObject* Obter(int x, int y);
    bool Adicionar(GameObject *object, int x, int y);
    bool Remover(GameObject *object);
    int Colunas() const { return _colunas;}
    int Linhas() const { return _linhas;}
};


#endif //CASTLEWARZ_MAPA_H
