//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_LIST_H
#define TRABALHO_LIST_H


#include "../../Comando.h"

class list : public Comando {
    string _letraColonia;
    int _indice;
public:
    list(Game *jogo);

    virtual bool VerificarArgs(std::stringstream &stringstream);

    virtual void Aplicar();
};


#endif //TRABALHO_LIST_H
