//
// Created by andre on 09/12/2016.
//

#include "repair.h"
#include "../../Game.h"
#include "../../Colonia.h"
#include "../../GameObject.h"
#include "../../Mensagem.h"
#include "../../GameObjects/Edificio.h"
repair::repair(Game *jogo) {
    _jogo = jogo;
    _nome = "repair";
}

bool repair::VerificarArgs(std::stringstream & stringstream) {
    int eid;

    if(!(stringstream >> eid))
        return false;

    /*Verificar se o EID existe*/
    Edificio *tmp = _jogo->_colonias[_jogo->_jogadorActual]->ObterEdificioPorEID(eid);
    if(tmp == nullptr)
    {
        _jogo->msg->Erro("ERRO! O EID indicado nao existe!\n");
        return false;
    }

    if(_jogo->_colonias[_jogo->_jogadorActual]->getDinheiro() < tmp->CustoReparacao())
    {
        _jogo->msg->Erro("ERRO! Nao tem dinheiro suficiente para reparar o edificio!\n");
    }

    _EID = eid;
    return true;

}

void repair::Aplicar()
{
    //Repara o edificio indicado (da colonia do jogador)
    Edificio *tmp = _jogo->_colonias[_jogo->_jogadorActual]->ObterEdificioPorEID(_EID);
    _jogo->_colonias[_jogo->_jogadorActual]->AumentarDinheiro(-tmp->CustoReparacao());
    tmp->Reparar();
    _jogo->msg->Sucesso("\tO edificio foi reparado com sucesso!\n");

}
