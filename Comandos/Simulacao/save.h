//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_SAVE_H
#define TRABALHO_SAVE_H


#include "../../Comando.h"

class save : public Comando
{
    string _nomeSave;
public:
    save(Game *jogo);

    bool VerificarArgs(std::stringstream &stringstream);

    void Aplicar();

};


#endif //TRABALHO_SAVE_H
