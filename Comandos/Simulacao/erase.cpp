//
// Created by andre on 09/12/2016.
//

#include "erase.h"
#include "../../Game.h"
#include "../../ExitType.h"

bool erase::VerificarArgs(std::stringstream &stringstream) {
    string nomeJogo;

    if(!(stringstream >> nomeJogo))
        return false;

    _nomeJogo = nomeJogo;
    return true;
}

void erase::Aplicar()
{
    _jogo->SairJogo(ExitType::ERASE,_nomeJogo);

}

erase::erase(Game *jogo) {
    _nome = "erase";
    _mudarJogador = true;
    _jogo = jogo;
}
