//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_BUILD_H
#define TRABALHO_BUILD_H


#include "../../Comando.h"

class build : public Comando {
    int _lin, _col, _EID;
    string _nomeEdificio;

public:
    build(Game *jogo);

    virtual bool VerificarArgs(std::stringstream &stringstream);

    virtual void Aplicar();


};


#endif //TRABALHO_BUILD_H
