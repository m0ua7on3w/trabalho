//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_RESTORE_H
#define TRABALHO_RESTORE_H


#include "../../Comando.h"

class restore : public Comando{
    string _nomeJogo;
public:
    restore(Game *jogo);

    virtual bool VerificarArgs(std::stringstream &stringstream);

    virtual void Aplicar();
};


#endif //TRABALHO_RESTORE_H
