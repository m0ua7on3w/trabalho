//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_FOCO_H
#define TRABALHO_FOCO_H

#include "../../Comando.h"

class foco : public Comando{
    int _lin, _col;

public:
    foco(Game *jogo);

    virtual bool VerificarArgs(std::stringstream &stringstream);

    virtual void Aplicar();
};


#endif //TRABALHO_FOCO_H
