//
// Created by andre on 09/12/2016.
//

#include <regex>
#include "list.h"
#include "../../Game.h"
#include "../../Colonia.h"
#include "../../Mensagem.h"

list::list(Game *jogo) {
    _jogo = jogo;
    _nome = "list";
}

bool list::VerificarArgs(std::stringstream &stringstream) {
    string letraColonia;

    if(!(stringstream >> letraColonia))
        return false;

    if(letraColonia.length() != 1)
        return false;

    if(!std::regex_match(letraColonia, regex("^[A-Za-z]+$"))) return false;

    bool encontrei = false;
    for(int i =0; i < _jogo->_colonias.size(); i++)
    {
        if(_jogo->_colonias[i]->get_nome() == letraColonia)
        {
            encontrei=true;
            _indice = i;
        }
    }
    if(!encontrei)
    {
        _jogo->msg->Erro("\nERRO! Nao existe colonia nenhuma com essa letra!\n");
        return false;
    }

    _letraColonia = letraColonia;
    return true;
}

void list::Aplicar()
{
    //Listar todas as informações sobre a colonia indicada
    std::cout << _jogo->_colonias[_indice];
}

