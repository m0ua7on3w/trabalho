//
// Created by andre on 09/12/2016.
//

#include <regex>
#include "setMoedas.h"
#include "../../Game.h"
#include "../../Colonia.h"
#include "../../Mensagem.h"

//Comando só para debug
setMoedas::setMoedas(Game *jogo) {
    _nome = "setmoedas";
    _jogo = jogo;
}

bool setMoedas::VerificarArgs(std::stringstream &stringstream) {
    string ccolonia;
    int numMoedas;

    if(!(stringstream >> ccolonia >> numMoedas))
        return false;

    if(ccolonia.length() != 1)
        return false;

    if(!std::regex_match(ccolonia, regex("^[A-Za-z]+$"))) return false;

    bool found=false;
    for(int i=0; i<_jogo->_colonias.size(); i++)
    {
        if(_jogo->_colonias[i]->get_nome() == ccolonia)
        {
            found = true;
            _indice = i;
        }
    }
    if(!found)
    {
        _jogo->msg->Erro("ERRO! Nao foi possivel encontrar a colonia indicada!\n");
        return false;
    }

    _colonia = ccolonia;
    _numMoedas = numMoedas;
    return true;
}

void setMoedas::Aplicar()
{
    //aplicar o num de moedas à colonia indicada
    _jogo->_colonias[_indice]->AumentarDinheiro(_numMoedas);
    _jogo->msg->Sucesso(("\tDinheiro atual: " + Mensagem::IntToString(_jogo->_colonias[_indice]->getDinheiro())) + "\n");

}


