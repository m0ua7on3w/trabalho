//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_LISTALLP_H
#define TRABALHO_LISTALLP_H


#include "../../Comando.h"

class listallp : public Comando
{
public:
    listallp(Game * jogo);

    virtual bool VerificarArgs(std::stringstream &stringstream);

    virtual void Aplicar();
};


#endif //TRABALHO_LISTALLP_H
