//
// Created by andre on 09/12/2016.
//

#include "restore.h"
#include "../../Game.h"
#include "../../ExitType.h"

restore::restore(Game *jogo)
{
    _jogo = jogo;
    _nome = "restore";
    _mudarJogador = true;
}

bool restore::VerificarArgs(std::stringstream &stringstream) {

    string nome;

    if(!(stringstream >> nome))
        return false;

    _nomeJogo = nome;
    return true;
}

void restore::Aplicar()
{
    _jogo->SairJogo(ExitType::RESTORE,_nomeJogo);
}
