//
// Created by andre on 09/12/2016.
//

#include "ataca.h"
#include "../../Game.h"
#include "../../Colonia.h"

ataca::ataca(Game *jogo) {
    _jogo = jogo;
    _nome = "ataca";
    _mudarJogador = true;
}


void ataca::Aplicar()
{
    //Actuar caracteristicas dos seres. Seres saem do castelo.
    _jogo->_colonias[_jogo->_jogadorActual]->Atuar();

}

bool ataca::VerificarArgs(std::stringstream & stringstream) {
    return true;
}
