//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_REPAIR_H
#define TRABALHO_REPAIR_H


#include "../../Comando.h"

class repair: public Comando
{
    int _EID, _indice;
public:
    repair(Game *jogo);

    virtual bool VerificarArgs(std::stringstream & stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_REPAIR_H
