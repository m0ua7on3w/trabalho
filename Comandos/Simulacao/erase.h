//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_ERASE_H
#define TRABALHO_ERASE_H


#include "../../Comando.h"

class erase : public Comando
{
    string _nomeJogo;

public:
    erase(Game *jogo);

    virtual bool VerificarArgs(std::stringstream &stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_ERASE_H
