//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_RECOLHE_H
#define TRABALHO_RECOLHE_H


#include "../../Comando.h"

class recolhe : public Comando{

public:
    recolhe(Game *jogo);

    virtual bool VerificarArgs(std::stringstream & stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_RECOLHE_H
