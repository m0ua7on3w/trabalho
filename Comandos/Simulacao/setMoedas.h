//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_SETMOEDAS_H
#define TRABALHO_SETMOEDAS_H


#include "../../Comando.h"

class setMoedas : public Comando{
private:
    string _colonia;
    int _numMoedas, _indice;
public:
    setMoedas(Game* jogo);
    virtual bool VerificarArgs(std::stringstream &stringstream);
    virtual void Aplicar();
};


#endif //TRABALHO_SETMOEDAS_H
