//
// Created by andre on 09/12/2016.
//

#include "fim.h"
#include "../../Game.h"

fim::fim(Game *jogo) {
    _jogo = jogo;
    _nome = "fim";
    _mudarJogador = true;
}

bool fim::VerificarArgs(std::stringstream &stringstream) {
    return true;
}

void fim::Aplicar()
{
    //TERMINA O JOGO!
    _jogo->_jogando = false;
}

