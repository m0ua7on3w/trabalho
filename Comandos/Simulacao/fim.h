//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_FIM_H
#define TRABALHO_FIM_H

#include "../../Comando.h"

class fim : public Comando{

public:
    //IM HERE!
    fim(Game *jogo);

    virtual bool VerificarArgs(std::stringstream &stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_FIM_H
