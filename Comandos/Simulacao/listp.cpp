//
// Created by andre on 09/12/2016.
//

#include <regex>
#include "listp.h"
#include "../../Game.h"
#include "../../Perfil.h"
#include "../../Mensagem.h"

listp::listp(Game *jogo) {
    _jogo = jogo;
    _nome = "listp";
}

bool listp::VerificarArgs(std::stringstream &stringstream) {
    string letraPerfil;

    if(!(stringstream >> letraPerfil))
        return false;

    if(letraPerfil.length() != 1)
        return false;

    if(!std::regex_match(letraPerfil, regex("^[A-Za-z]+$"))) return false;

    bool found = false;
    for(int i = 0; i<_jogo->_perfisTotais.size(); i++)
    {
        if(_jogo->_perfisTotais[i]->ObterLetraPerfil() == letraPerfil)
        {
            found=true;
            _indice = i;
        }
    }

    if(!found)
    {
        _jogo->msg->Erro("\nERRO! Nao foi possivel encontrar o perfil dado.\n");
        return false;
    }
    _letraPerfil = letraPerfil;
    return true;
}

void listp::Aplicar() {
    //LISTA DE CARACTERISTICAS
    //INCLUINDO OS VALORES CUMULATIVOS DA DEFESA, ATAQUE E VELOCIDADE
    std::cout << _jogo->_perfisTotais[_indice];
}

