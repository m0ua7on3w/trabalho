//
// Created by andre on 09/12/2016.
//

#include "load.h"
#include "sys/stat.h"
#include "../../Mensagem.h"
#include "../../Game.h"
#include <fstream>
#include <cstdlib>
#include "../../ScreenGame.h"

load::load(Game *jogo) {
    _jogo = jogo;
    _nome = "load";
}

bool load::VerificarArgs(std::stringstream &stringstream) {
    string nomeFicheiro;
    struct stat buffer;

    if(!(stringstream >> nomeFicheiro))
        return false;

    if(nomeFicheiro.length() < 4)
        nomeFicheiro+=".txt";

    if((nomeFicheiro.substr(nomeFicheiro.length()-4)) != ".txt")
        nomeFicheiro+=".txt";

    //Ficheiro nao existe
    if(stat (nomeFicheiro.c_str(), &buffer) != 0)
    {
        _jogo->msg->Erro(( "ERRO! O nome de ficheiro indicado nao foi encontrado!\n"));
        return false;
    }

    _nomeFicheiro = nomeFicheiro;
    return true;
}

void load::Aplicar()
{
    ifstream dados(_nomeFicheiro);

    string linha;
    if (dados.is_open())
    {
        while (!dados.eof()) {
            // ler string com os dados
            getline(dados, linha);
            stringstream line(linha);

            if(!_jogo->Play(line)) {
                _jogo->_jogando = true;
                break;
            }
        }
        dados.close();
        system("pause");
        _jogo->scrGame->Draw();
    } else
        _jogo->msg->Erro(( "ERRO! Ocorreu um erro ao abrir o ficheiro!\n"));

}