//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_UPGRADE_H
#define TRABALHO_UPGRADE_H


#include "../../Comando.h"

class upgrade : public Comando
{
    int _EID;

public:
    upgrade(Game * jogo);

    virtual bool VerificarArgs(std::stringstream & stringstream);

    virtual void Aplicar();


};


#endif //TRABALHO_UPGRADE_H
