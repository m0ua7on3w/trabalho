//
// Created by andre on 09/12/2016.
//

#include "sell.h"
#include "../../Game.h"
#include "../../GameObjects/Edificio.h"
#include "../../Colonia.h"
#include "../../Mensagem.h"

sell::sell(Game *jogo) {
    _jogo = jogo;
    _nome = "sell";
}

bool sell::VerificarArgs(std::stringstream & stringstream) {
    int eid;

    if(!(stringstream >> eid))
        return false;

    //Verificar se o EID existe para o vender
    Edificio *tmp = _jogo->_colonias[_jogo->_jogadorActual]->ObterEdificioPorEID(eid);
    if(tmp == nullptr)
    {
        _jogo->msg->Erro("ERRO! O EID indicado nao existe!\n");
        return false;
    }

    _EID = eid;
    return true;
}

void sell::Aplicar()
{
    //remove/vende o edificio da colonia do jogador
    Edificio *tmp =_jogo->_colonias[_jogo->_jogadorActual]->ObterEdificioPorEID(_EID);
    int refund = (tmp->getCustoTotal())/2;
    if(_jogo->_colonias[_jogo->_jogadorActual]->Eliminar(tmp))
    {
         _jogo->_colonias[_jogo->_jogadorActual]->AumentarDinheiro(refund);
        _jogo->msg->Sucesso("\tO edificio foi vendido com sucesso! \n\tRefund: " + Mensagem::IntToString(refund) +"!\n");
    }
    else
        _jogo->msg->Erro("ERRO! Ocorreu um erro ao vender o edificio!\n");


}
