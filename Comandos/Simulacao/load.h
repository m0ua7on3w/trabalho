//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_LOAD_H
#define TRABALHO_LOAD_H


#include "../../Comando.h"

class load : public Comando
{
    string _nomeFicheiro;

public:
    load(Game *jogo);

    virtual bool VerificarArgs(std::stringstream &stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_LOAD_H
