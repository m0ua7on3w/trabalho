//
// Created by andre on 09/12/2016.
//

#include "upgrade.h"
#include "../../Game.h"
#include "../../Colonia.h"
#include "../../GameObject.h"
#include "../../Mapa.h"
#include "../../Mensagem.h"
#include "../../GameObjects/Edificio.h"

upgrade::upgrade(Game *jogo) {
    _jogo = jogo;
    _nome = "upgrade";
}


bool upgrade::VerificarArgs(std::stringstream & stringstream) {
    int eid;

    if(!(stringstream >> eid))
        return false;

    /*Verificar se o EID existe*/

    Edificio *tmp = _jogo->_colonias[_jogo->_jogadorActual]->ObterEdificioPorEID(eid);
    if(tmp == nullptr)
    {
        _jogo->msg->Erro("ERRO! O EID indicado nao existe!\n");
        return false;
    }

    //Verificar se o utilizador tem dinheiro para comprar, cada upgrade custa 10
    if(_jogo->_colonias[_jogo->_jogadorActual]->getDinheiro() < 10)
    {
        _jogo->msg->Erro("ERRO! Nao tem dinheiro suficiente para melhorar o edificio!\n");
        _jogo->msg->Info(("\tDinheiro atual: " + Mensagem::IntToString(_jogo->_colonias[_jogo->_jogadorActual]->getDinheiro()) + " | Custo melhorar: 10\n"));
        return false;
    }

    _EID = eid;
    return true;
}

void upgrade::Aplicar()
{
    Edificio *ed =_jogo->_colonias[_jogo->_jogadorActual]->ObterEdificioPorEID(_EID);
    _jogo->_colonias[_jogo->_jogadorActual]->AumentarDinheiro(-10);
    ed->Melhorar();
    string str = ed->ObterNome();
    if(str == "Q")
        str+= "uinta";
    else if(str == "T")
        str+="orre";
    _jogo->msg->Sucesso("\n\tA "+ str + " com o EID:"+Mensagem::IntToString(_EID)+" foi melhorada com sucesso!\n");

}
