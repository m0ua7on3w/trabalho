//
// Created by andre on 09/12/2016.
//

#include "build.h"
#include "../../GameObjects/Edificios/Torre.h"
#include "../../GameObjects/Edificios/Quinta.h"
#include "../../Game.h"
#include "../../Mapa.h"
#include "../../Colonia.h"
#include "../../Ponto.h"
#include "../../Mensagem.h"

build::build(Game *jogo) {
    _jogo = jogo;
    _nome = "build";
}

bool build::VerificarArgs(std::stringstream &stringstream) {
    int linhas, colunas;
    string edificio;

    if(!(stringstream >> edificio >> linhas >> colunas))
        return false;

    if(linhas >= _jogo->_mLinhas || linhas < 0)
    {
        _jogo->msg->Erro("ERRO! Nao pode construir algo fora do mapa!\n");
        return false;
    }

    if(colunas < 0 || colunas >= _jogo->_mColunas)
    {
        _jogo->msg->Erro("ERRO! Nao pode construir algo fora do mapa!\n");
        return false;
    }
    GameObject *tmp =_jogo->_mapa->Obter(colunas, linhas);
    if( tmp != nullptr)
    {
        _jogo->msg->Erro("\nERRO! O espaco pretendido esta ocupado!\n");
        return false;

    }

    if(edificio == "castelo")
    {
        _jogo->msg->Erro("\nERRO! Os castelos nao podem ser construidos!!\n");
        return false;
    }
    else if(edificio == "torre")
    {
        //Ver o custo do edificio com o dinheiro que o player tem acutalmente
        if(_jogo->_colonias[_jogo->_jogadorActual]->getDinheiro() < 30)
        {
            _jogo->msg->Erro("\nNao tem dinheiro suficiente para construir a torre!\n");
            return false;
        }
    }
    else if(edificio == "quinta")
    {
        if(_jogo->_colonias[_jogo->_jogadorActual]->getDinheiro() < 20)
        {
            _jogo->msg->Erro("\nNao tem dinheiro suficiente para construir a quinta!\n");
            return false;
        }
    }
    else
    {
        _jogo->msg->Erro("\nERRO! O edificio dado nao existe!\n");
        return false;
    }

    //Verificar que ao criar o edificio não vai estar a mais de 10 posicoes de distancia do casteloC
    if(_jogo->_colonias[_jogo->_jogadorActual]->DistanciaDoCastelo(new Ponto(colunas,linhas)) > 10)
    {
        _jogo->msg->Erro("\nO edificio a construir nao pode estar a mais de 10 posicoes do castelo!\n");
        _jogo->msg->Info(("\tO seu castelo encontra-se em  ("
                         + Mensagem::IntToString(_jogo->c[_jogo->_jogadorActual]->c)
                         + ","
                         + Mensagem::IntToString(_jogo->c[_jogo->_jogadorActual]->l)) + ").\n");
        return false;
    }

    _lin = linhas;
    _col = colunas;
    _nomeEdificio = edificio;

    return true;
}

void build::Aplicar()
{
    Ponto *p = new Ponto(_col, _lin);
    Edificio * ed = nullptr;
    int tipo;
    if(_nomeEdificio == "torre")
        tipo = 1;
    else if(_nomeEdificio == "quinta")
            tipo = 2;

    ed = _jogo->_colonias[_jogo->_jogadorActual]->CriarEdificio(p, tipo);

    if (ed!= nullptr)
        _jogo->msg->Sucesso("\n\t" + _nomeEdificio + " criada em (" + Mensagem::IntToString(_lin) + "," + Mensagem::IntToString(_col) + ")"
                            + " - EID: " + Mensagem::IntToString(ed->ObterEID())
                            +"\n");
}
