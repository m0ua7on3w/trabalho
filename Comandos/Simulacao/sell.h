//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_SELL_H
#define TRABALHO_SELL_H


#include "../../Comando.h"

class sell : public Comando{
    int _EID;

public:
    sell(Game *jogo);

    virtual bool VerificarArgs(std::stringstream & stringstream);

    virtual void Aplicar();


};


#endif //TRABALHO_SELL_H
