//
// Created by andre on 09/12/2016.
//

#include "save.h"
#include "../../Game.h"
#include "../../ExitType.h"

save::save(Game *jogo) {
    _jogo = jogo;
    _nome = "save";
    _mudarJogador = true;
}

bool save::VerificarArgs(std::stringstream &stringstream) {
    string nomeJogo;

    if(!(stringstream >> nomeJogo))
        return false;

    _nomeSave = nomeJogo;

    return true;
}

void save::Aplicar()
{
    _jogo->SairJogo(ExitType::SAVE,_nomeSave);
}
