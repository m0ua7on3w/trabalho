//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_NEXTN_H
#define TRABALHO_NEXTN_H


#include "../../Comando.h"

class nextn : public Comando{
    int _numInstantes;

public:
    nextn(Game *jogo);

    virtual bool VerificarArgs(std::stringstream & stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_NEXTN_H
