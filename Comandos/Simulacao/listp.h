//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_LISTP_H
#define TRABALHO_LISTP_H


#include "../../Comando.h"

class listp : public Comando{
    string _letraPerfil;
    int _indice;
public:
    listp(Game *jogo);

    virtual bool VerificarArgs(std::stringstream &stringstream);

    virtual void Aplicar();
};


#endif //TRABALHO_LISTP_H
