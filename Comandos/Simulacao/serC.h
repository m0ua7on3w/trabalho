//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_SERC_H
#define TRABALHO_SERC_H


#include "../../Comando.h"

class serC : public Comando{
    int _numeroSeres,_indicePerfil;
    string _letraPerfil;

public:
    serC(Game *jogo);

    virtual bool VerificarArgs(std::stringstream & stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_SERC_H
