//
// Created by andre on 09/12/2016.
//

#include "foco.h"
#include "../../Game.h"
#include "../../Mapa.h"
#include "../../ScreenGame.h"
#include "../../Mensagem.h"
foco::foco(Game *jogo) {
    _jogo = jogo;
    _nome = "foco";
}

bool foco::VerificarArgs(std::stringstream &stringstream) {
    int linha, coluna;

    if(!(stringstream >> linha >> coluna))
        return false;

    if(linha >= _jogo->_mLinhas || linha < 0)
    {
        _jogo->msg->Erro("ERRO! Nao pode mudar o foco para fora do mapa\n");
        return false;
    }

    if(coluna < 0 || coluna >= _jogo->_mColunas)
    {
        _jogo->msg->Erro("ERRO! Nao pode mudar o foco para fora do mapa\n");
        return false;
    }

    _lin = linha;
    _col = coluna;
    return true;
}

void foco::Aplicar()
{

    _jogo->scrGame->MudarFoco(_lin,_col);
    _jogo->scrGame->Draw();


}


