//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_ATACA_H
#define TRABALHO_ATACA_H


#include "../../Comando.h"

class ataca : public Comando{

public:

    ataca(Game *jogo);
    virtual bool VerificarArgs(std::stringstream &stringstream);

    virtual void Aplicar();
};


#endif //TRABALHO_ATACA_H
