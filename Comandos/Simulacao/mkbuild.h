//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_MKBUILD_H
#define TRABALHO_MKBUILD_H


#include "../../Comando.h"

class mkbuild : public Comando{
    string _nomeEdificio, _nomeColonia;
    int _lin, _col, _indiceColonia;
public:
    mkbuild(Game *jogo);

    virtual bool VerificarArgs(std::stringstream &stringstream);

    virtual void Aplicar();
};


#endif //TRABALHO_MKBUILD_H
