//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_NEXT_H
#define TRABALHO_NEXT_H


#include "../../Comando.h"

class nextC : public Comando{

public:
    nextC(Game *jogo);

    virtual bool VerificarArgs(std::stringstream & stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_NEXT_H
