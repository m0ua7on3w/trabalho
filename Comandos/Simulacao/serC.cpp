//
// Created by andre on 09/12/2016.
//

#include <regex>
#include "serC.h"
#include "../../Game.h"
#include "../../Perfil.h"
#include "../../Colonia.h"
#include "../../Mensagem.h"

serC::serC(Game *jogo)
{
    _jogo = jogo;
    _nome = "ser";
}

bool serC::VerificarArgs(std::stringstream & stringstream) {
    int numSeres;
    string letraPerfil;

    if(!(stringstream >> numSeres >> letraPerfil))
        return false;

    if(!std::regex_match(letraPerfil, regex("^[A-Za-z]+$"))) return false;

    bool found =false;

    for(int i = 0; i<_jogo->_colonias[_jogo->_jogadorActual]->_perfis.size();i++)
    {
        if(_jogo->_colonias[_jogo->_jogadorActual]->_perfis[i]->ObterLetraPerfil() == letraPerfil)
        {
            found =true;
            _indicePerfil = i;
        }
    }
    if(!found)
    {
        _jogo->msg->Erro("ERRO! Nao foi possivel encontrar o perfil indicado!\n");
        return false;
    }

    if(_jogo->_colonias[_jogo->_jogadorActual]->getDinheiro() < (_jogo->_colonias[_jogo->_jogadorActual]->_perfis[_indicePerfil]->getCustoTotal()*numSeres))
    {
        _jogo->msg->Erro(("\tDinheiro atual:" +
                                 Mensagem::IntToString(_jogo->_colonias[_jogo->_jogadorActual]->getDinheiro()) +
                                 " | Custo total:" +
                                 Mensagem::IntToString((_jogo->_colonias[_jogo->_jogadorActual]->_perfis[_indicePerfil]->getCustoTotal()*numSeres)) + "\n"));
        _jogo->msg->Erro("ERRO! Nao tem dinheiro suficiente para criar " + Mensagem::IntToString(numSeres) + " seres!\n");
        return false;
    }


    _numeroSeres = numSeres;
    _letraPerfil = letraPerfil;
    return true;
}

void serC::Aplicar()
{
    for(int i=0; i<_numeroSeres; i++)
    {
        _jogo->_colonias[_jogo->_jogadorActual]->CriarSer(_indicePerfil);
    }
    _jogo->msg->Sucesso(("\n\tForam criados " + Mensagem::IntToString(_numeroSeres) + " seres com o perfil '" + _jogo->_colonias[_jogo->_jogadorActual]->_perfis[_indicePerfil]->ObterLetraPerfil() + "'.\n"));
}
