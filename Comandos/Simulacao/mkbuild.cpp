//
// Created by andre on 09/12/2016.
//

#include <regex>
#include "mkbuild.h"
#include "../../Game.h"
#include "../../Mapa.h"
#include "../../Colonia.h"
#include "../../Ponto.h"
#include "../../Mensagem.h"

//Usamos isto apenas para debug, nao num jogo a sério!
mkbuild::mkbuild(Game *jogo)
{
    _jogo = jogo;
    _nome = "mkbuild";
}

bool mkbuild::VerificarArgs(std::stringstream & stringstream) {
    int linhas, colunas;
    string nomeED, nomeColonia;

    if(!(stringstream >> nomeED >> linhas >> colunas >> nomeColonia))
        return false;

    if(linhas > _jogo->_mLinhas || linhas < 0 || colunas < 0 || colunas > _jogo->_mColunas)
        return false;

    if(nomeED != "quinta" && nomeED !="torre")
        return false;

    if(!std::regex_match(nomeColonia, regex("^[A-Za-z]+$"))) return false;

    bool found =false;

    for(int i = 0; i<_jogo->_colonias.size();i++)
    {
        if(_jogo->_colonias[i]->get_nome() == nomeColonia)
        {
            found =true;
            _indiceColonia = i;
        }
    }
    if(!found)
    {
        _jogo->msg->Erro("\nERRO! Nao existe colonia nenhuma com a letra dada!\n");
        return false;
    }
    _lin = linhas;
    _col = colunas;
    _nomeColonia = nomeColonia;
    _nomeEdificio = nomeED;
    return true;
}

void mkbuild::Aplicar()
{
    //Adiciona um edificio pertencendo à colonia indicada
    if (_nomeEdificio == "quinta") {
        if (_jogo->_colonias[_indiceColonia]->CriarEdificio(new Ponto(_col, _lin), 2) != nullptr)
            _jogo->msg->Sucesso((
                    "\n\tQuinta criada em (" + Mensagem::IntToString(_lin) + "," + Mensagem::IntToString(_col) +
                    ") para a colonia '" + _nomeColonia + "'!\n"));
        else
            _jogo->msg->Erro("\nERRO! Ocorreu um erro ao criar a quinta! CriarEdificio devolveu nullptr.\n");

    } else if (_nomeEdificio == "torre") {
        if (_jogo->_colonias[_indiceColonia]->CriarEdificio(new Ponto(_col, _lin), 1) != nullptr)
            _jogo->msg->Sucesso((
                    "\n\tTorre criada em (" + Mensagem::IntToString(_lin) + "," + Mensagem::IntToString(_col) +
                    ") para a colonia '" + _nomeColonia + "'!\n"));
        else
            _jogo->msg->Erro("\nERRO! Ocorreu um erro ao criar a torre! CriarEdificio devolveu nullptr.\n");

    }
}
