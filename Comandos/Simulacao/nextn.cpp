//
// Created by andre on 09/12/2016.
//

#include "nextn.h"
#include "../../Colonia.h"
#include "../../Game.h"


nextn::nextn(Game *jogo) {
    _jogo = jogo;
    _nome = "nextn";
    _mudarJogador=true;
}

bool nextn::VerificarArgs(std::stringstream & stringstream) {
    int ninstantes;

    if(!(stringstream >> ninstantes))
        return false;

    _numInstantes = ninstantes;
    return true;
}

void nextn::Aplicar()
{
    //Avança _numInstantes
    _jogo->_colonias[_jogo->_jogadorActual]->SetNext(_numInstantes);

}

