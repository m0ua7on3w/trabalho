//
// Created by andre on 09/12/2016.
//

#ifndef TRABALHO_ZOOMOUT_H
#define TRABALHO_ZOOMOUT_H


#include "../../Comando.h"

class zoomout : public Comando
{

public:
    zoomout(Game *jogo);

    bool VerificarArgs(std::stringstream &stringstream);

    void Aplicar() override;

};


#endif //TRABALHO_ZOOMOUT_H
