//
// Created by andre on 09/12/2016.
//

#include "recolhe.h"
#include "../../Colonia.h"
#include "../../Game.h"

recolhe::recolhe(Game *jogo) {
    _jogo = jogo;
    _nome = "recolhe";
    _mudarJogador = true;
}

bool recolhe::VerificarArgs(std::stringstream & stringstream)
{
    return true;
}

void recolhe::Aplicar()
{
    _jogo->_colonias[_jogo->_jogadorActual]->Recuar();
}

