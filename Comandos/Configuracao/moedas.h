//
// Created by andre on 13/12/2016.
//

#ifndef TRABALHO_MOEDAS_H
#define TRABALHO_MOEDAS_H


#include "../../Comando.h"

class moedas : public Comando
{
    int _numMoedas;

public:
    moedas(Game *jogo);

    virtual bool VerificarArgs(stringstream &stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_MOEDAS_H
