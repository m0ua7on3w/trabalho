//
// Created by andre on 14/12/2016.
//

#include <sys/stat.h>
#include <fstream>
#include "loadFile.h"
#include "../../Game.h"
#include "../../Configuracao.h"
#include "../../Mensagem.h"


loadFile::loadFile(Game *jogo) {
    _jogo =jogo;
    _nome = "load";
}

bool loadFile::VerificarArgs(stringstream &stringstream) {
    _mudarJogador = false;
    string nomeFicheiro;

    struct stat buffer;

    if(!(stringstream >> nomeFicheiro))
        return false;

    if(nomeFicheiro.length() < 4)
        nomeFicheiro+=".txt";

    if((nomeFicheiro.substr(nomeFicheiro.length()-4)) != ".txt")
        nomeFicheiro+=".txt";

    //Ficheiro nao existe
    if(stat (nomeFicheiro.c_str(), &buffer) != 0)
    {
        _jogo->msg->Erro(( "ERRO! O nome de ficheiro indicado nao foi encontrado!\n"));
        return false;
    }

    _nomeFicheiro = nomeFicheiro;
    return true;
}

void loadFile::Aplicar()
{
    ifstream dados(_nomeFicheiro);

    string linha;
    if (dados.is_open())
    {
        while (!dados.eof()) {
            // ler string com os dados
            getline(dados, linha);
            stringstream line(linha);

            if(!_jogo->Configurar(line)) {
                _mudarJogador = true;
                break;
            }

        }
        dados.close();
        std::cout << endl;
    } else
        _jogo->msg->Erro(( "ERRO! Ocorreu um erro ao abrir o ficheiro!\n"));

}

