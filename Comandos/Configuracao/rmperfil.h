//
// Created by andre on 13/12/2016.
//

#ifndef TRABALHO_RMPERFIL_H
#define TRABALHO_RMPERFIL_H


#include "../../Comando.h"

class rmperfil : public Comando{
    string _letra;
public:
    rmperfil(Game *jogo);

    virtual bool VerificarArgs(stringstream &stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_RMPERFIL_H
