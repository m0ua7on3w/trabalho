//
// Created by andre on 13/12/2016.
//

#include <regex>
#include "subperfil.h"
#include "../../Perfil.h"
#include "../../Caracteristica.h"
#include "../../Game.h"
#include "../../Mensagem.h"

subperfil::subperfil(Game *jogo) {
    _jogo = jogo;
    _nome = "subperfil";
}

bool subperfil::VerificarArgs(stringstream &stringstream) {
    string letra;
    int id;

    if(!(stringstream >> letra >> id))
        return false;

    //Verificar letra
    if(letra.length() != 1)
        return false;

    if(!std::regex_match(letra, regex("^[A-Za-z]+$"))) return false;

    if(id < 1 || id > 14)
    {
        _jogo->msg->Erro("ERRO! Nao foi possivel encontrar a caracteristica indicada!\n");
        return false;
    }

    bool t = false;

    for(int i=0; i<_jogo->_perfisJogador.size(); i++)
    {
        //Verificar se a letra dada existe dentro dos perfis já criados;
        if(_jogo->_perfisJogador[i]->ObterLetraPerfil() == letra)
        {
            _indice = i;
            t=true;
        }
    }

    if(!t)
    {
        _jogo->msg->Erro("ERRO! Nao foi possivel encontrar o perfil dado.\n");
        return false;
    }

    if(!_jogo->_perfisJogador[_indice]->CheckCaracteristicaExiste(id))
    {
        _jogo->msg->Erro("ERRO! Impossivel encontrar a caracteristica.\n");
        return false;
    }

    _letra = letra;
    _id = id;
    return true;
}

void subperfil::Aplicar()
{
    if(_jogo->_perfisJogador[_indice]->RemoverCaracteristica(_id))
        _jogo->msg->Sucesso(("\tA caracteristica com o id:" + Mensagem::IntToString(_id) + " foi removida do perfil '" + _letra + "' com sucesso!\n"));
    else
        _jogo->msg->Erro("ERRO! Nao foi possivel remover a caracteristica!\n");
}
