//
// Created by andre on 13/12/2016.
//

#ifndef TRABALHO_OPONENTES_H
#define TRABALHO_OPONENTES_H


#include "../../Comando.h"

class oponentes : public Comando
{
    int _numOpo;

public:
    oponentes(Game *jogo);

    virtual bool VerificarArgs(stringstream &stringstream);

    virtual void Aplicar();
    //char NumeroParaLetra(int n);
};


#endif //TRABALHO_OPONENTES_H
