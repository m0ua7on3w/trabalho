//
// Created by andre on 13/12/2016.
//

#ifndef TRABALHO_INICIO_H
#define TRABALHO_INICIO_H


#include "../../Comando.h"
class inicio : public Comando{
public:
    inicio(Game *jogo);
    bool VerificarArgs(stringstream &stringstream);

    void Aplicar() override;
};


#endif //TRABALHO_INICIO_H
