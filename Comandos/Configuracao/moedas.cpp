//
// Created by andre on 13/12/2016.
//

#include "moedas.h"
#include "../../Game.h"
#include "../../Mensagem.h"

bool moedas::VerificarArgs(stringstream &stringstream) {
    int num;

    if(!(stringstream >> num))
        return false;

    if(num <= 0)
    {
        _jogo->msg->Erro("ERRO! As moedas devem ser positivas.\n");
        return false;
    }
    _numMoedas = num;
    return true;
}

void moedas::Aplicar()
{
    _jogo->_moedasIniciais = _numMoedas;
}

moedas::moedas(Game *jogo)
{
    _jogo = jogo;
    _nome = "moedas";
}
