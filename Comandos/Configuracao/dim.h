//
// Created by andre on 13/12/2016.
//

#ifndef TRABALHO_DIM_H
#define TRABALHO_DIM_H


#include "../../Comando.h"

class dim : public Comando
{
    int _linhas, _colunas;

public:
    dim(Game *jogo);

    virtual bool VerificarArgs(stringstream &stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_DIM_H
