//
// Created by andre on 13/12/2016.
//

#ifndef TRABALHO_CASTELO_H
#define TRABALHO_CASTELO_H


#include "../../Comando.h"

class casteloC : public Comando
{
    string _letraColonia;
    int _linha, _coluna;

public:
    casteloC(Game *jogo);

    virtual bool VerificarArgs(stringstream &stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_CASTELO_H
