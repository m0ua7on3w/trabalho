//
// Created by andre on 13/12/2016.
//

#include "addperfil.h"
#include <regex>
#include "../../Perfil.h"
#include "../../Caracteristica.h"
#include "../../Game.h"
#include "../../Mensagem.h"
#include "../../Caracteristicas/Bandeira.h"
#include "../../Caracteristicas/Agressao.h"
#include "../../Caracteristicas/Armadura.h"
#include "../../Caracteristicas/CaracteristicasLigadas/Aluno.h"
#include "../../Caracteristicas/BuildSeeker.h"
#include "../../Caracteristicas/Ecologico.h"
#include "../../Caracteristicas/Faca.h"
#include "../../Caracteristicas/HeatSeeker.h"
#include "../../Caracteristicas/PeleDura.h"
#include "../../Caracteristicas/Remedio.h"
#include "../../Caracteristicas/SecondChance.h"
#include "../../Caracteristicas/Superior.h"
#include "../../Caracteristicas/Walker.h"
#include "../../Caracteristicas/CaracteristicasLigadas/Espada.h"

addperfil::addperfil(Game *jogo)
{
    _jogo = jogo;
    _nome = "addperfil";
}

bool addperfil::VerificarArgs(stringstream &stringstream) {
    int id;
    string letra;

    if(!(stringstream >> letra >> id))
        return false;

    //Verificar letra
    if(letra.length() != 1) return false;

    if(!std::regex_match(letra, regex("^[A-Za-z]+$"))) return false;

    if(id < 1 || id > 14)
    {
        _jogo->msg->Erro("ERRO! Nao foi possivel encontrar a caracteristica indicada!\n");
        return false;
    }

    bool t = false;

    for(int i=0; i<_jogo->_perfisJogador.size(); i++)
    {
        //Verificar se a letra dada existe dentro dos perfis já criados;
        if(_jogo->_perfisJogador[i]->ObterLetraPerfil() == letra)
        {
            _indice = i;
            t=true;
        }
    }
    if(!t)
    {
        _jogo->msg->Erro("ERRO! Nao foi possivel encontrar o perfil indicado!\n");
        return false;
    }

    if(_jogo->_perfisJogador[_indice]->getMaxForca() <= 0)
    {
        _jogo->msg->Erro(("ERRO! Nao foi possivel adicionar a caracteristica (" + Mensagem::IntToString(id) + ") pedida ao perfil '" + letra + "' por falta de forca.\n" ));
        return false;
    }
    _id = id;
    return true;
}

void addperfil::Aplicar() {
    Caracteristica *c;
    int ataque=0, defesa=0, velocidade=0, vida =0;

    c= nullptr;
    switch (_id)
    {
        case 1:
            c = new Bandeira();break;
        case 2:
            c = new Superior();
            vida++;
            break;
        case 3:
            c = new PeleDura();
            defesa++;
            break;
        case 4:
            c = new Armadura();
            defesa += 2;
            break;
        case 5:
            c = new Faca();
            ataque ++;
            break;
        case 6:
            c = new Espada();
            ataque += 3;
            break;
        case 7:
            c = new Agressao();break;
        case 8:
            c = new Ecologico();break;
        case 9:
            c = new HeatSeeker();
            velocidade++;
            break;
        case 10:
            c = new BuildSeeker();
            velocidade++;
            break;
        case 11:
            c = new Walker();
            velocidade++;
            break;
        case 12:
            c = new Remedio();break;
        case 13:
            c = new SecondChance();break;
        case 14:
            c = new Aluno();break;

        default:break;
    }
    if(c== nullptr)
        delete c;
    else
        if(_jogo->_perfisJogador[_indice]->AdicionarCaracteristica(c))
        {
            _jogo->_perfisJogador[_indice]->AdicionarValoresCumulativos(ataque, defesa, velocidade, vida);
            _jogo->msg->Sucesso(("\t" + c->get_nome() + " adicionada ao perfil '" + _jogo->_perfisJogador[_indice]->ObterLetraPerfil() + "' com sucesso!"  + "\n"));
            _jogo->msg->Info("\tForca restante: " + Mensagem::IntToString(_jogo->_perfisJogador[_indice]->getMaxForca()) + ".\n");

        }
        else
        {
            _jogo->msg->Erro(("\nERRO! Nao foi possivel adicionar '" + c->get_nome() + "' ao perfil '" +  _jogo->_perfisJogador[_indice]->ObterLetraPerfil() + "' por falta de forca.\n"));
        }
}

