//
// Created by andre on 13/12/2016.
//

#include "dim.h"
#include "../../Game.h"
#include "../../Mensagem.h"

bool dim::VerificarArgs(stringstream &stringstream) {
    int linhas,colunas;

    if(!(stringstream >> linhas >> colunas))
        return false;

    if(linhas <= 0 || colunas <= 0)
    {
        _jogo->msg->Erro(("ERRO! Introduza um valor correcto para as linhas e para as colunas!\n"));
        return false;
    }

    _linhas = linhas;
    _colunas = colunas;
    return true;
}

void dim::Aplicar()
{
    _jogo->_mLinhas = _linhas;
    _jogo->_mColunas = _colunas;
}

dim::dim(Game *jogo)
{
    _jogo = jogo;
    _nome = "dim";
}
