//
// Created by andre on 13/12/2016.
//

#ifndef TRABALHO_SUBPERFIL_H
#define TRABALHO_SUBPERFIL_H


#include "../../Comando.h"

class subperfil : public Comando{
    string _letra;
    int _id;
    int _indice;

public:
    subperfil(Game *jogo);

    virtual bool VerificarArgs(stringstream &stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_SUBPERFIL_H
