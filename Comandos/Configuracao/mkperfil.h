//
// Created by andre on 13/12/2016.
//

#ifndef TRABALHO_MKPERFIL_H
#define TRABALHO_MKPERFIL_H


#include "../../Comando.h"

class mkperfil : public Comando
{
    string _letraDada;

public:
    mkperfil(Game *jogo);

    virtual bool VerificarArgs(stringstream &stringstream);

    virtual void Aplicar();


};


#endif //TRABALHO_MKPERFIL_H
