//
// Created by andre on 13/12/2016.
//

#ifndef TRABALHO_ADDPERFIL_H
#define TRABALHO_ADDPERFIL_H


#include "../../Comando.h"

class addperfil : public Comando{
    int _id,_indice;

public:
    addperfil(Game *jogo);
    virtual bool VerificarArgs(stringstream &stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_ADDPERFIL_H
