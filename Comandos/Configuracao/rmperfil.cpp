//
// Created by andre on 13/12/2016.
//

#include <regex>
#include "rmperfil.h"
#include "../../Perfil.h"
#include "../../Game.h"
#include "../../Mensagem.h"


rmperfil::rmperfil(Game *jogo) {
    _nome ="rmperfil";
    _jogo = jogo;
}

bool rmperfil::VerificarArgs(stringstream &stringstream)
{
    string letra;

    if(!(stringstream >> letra))
        return false;

    if(letra.length() != 1) return false;

    if(!std::regex_match(letra, regex("^[A-Za-z]+$"))) return false;

    bool f = false;
    for(int i=0; i<_jogo->_perfisJogador.size(); i++)
    {
        //Verificar se o perfil existe
        if(_jogo->_perfisJogador[i]->ObterLetraPerfil() == letra)
        {
            f=true;
        }
    }

    if(!f)
    {
        _jogo->msg->Erro("ERRO! O perfil indicado a remover nao existe!\n");
        return false;
    }
    _letra = letra;
    return true;
}

void rmperfil::Aplicar()
{
   if(_jogo->ApagarPerfil(_letra))
       _jogo->msg->Sucesso(("\tO perfil com a letra '" + _letra + "' foi removido com sucesso!\n"));
    else
       _jogo->msg->Erro("ERRO! Foi encontrado um erro ao remover o perfil!\n");
}


