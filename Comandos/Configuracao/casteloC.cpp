//
// Created by andre on 13/12/2016.
//

#include "casteloC.h"
#include "../../Game.h"
#include "../../Mensagem.h"
#include <regex>
#include <string>

casteloC::casteloC(Game *jogo) {
    _jogo = jogo;
    _nome = "castelo";
}

bool casteloC::VerificarArgs(stringstream &stringstream) {
    string letra;
    int linha, coluna;

    //Se ainda nao tiverem sido dadas linhas, não é possivel continuar
    if(_jogo->_mLinhas == 0 || _jogo->_mColunas == 0)
    {
        _jogo->msg->Erro(("ERRO! O comando DIM ainda nao foi configurado!\n"));
        return false;
    }

    if(_jogo->_numJogadores == 0)
    {
        _jogo->msg->Erro(("ERRO! O comando OPONENTES ainda nao foi configurado!\n"));
        return false;
    }

    if(!(stringstream >> letra >> linha >> coluna)) return false;

    if(letra.length() != 1) return false;

    if(!std::regex_match(letra, regex("^[A-Za-z]+$"))) return false;

    if(linha >= _jogo->_mLinhas)
    {
        _jogo->msg->Erro(("ERRO! Nao pode mudar o castelo para fora do mapa!\n"));
        return false;
    }

    if(coluna >= _jogo->_mColunas)  {
        _jogo->msg->Erro(("ERRO! Nao pode mudar o castelo para fora do mapa!!\n"));
        return false;
    }

    _letraColonia = letra;
    _linha = linha;
    _coluna = coluna;

    return true;
}

void casteloC::Aplicar()
{
    _jogo->MudarPosicaoCastelo(_letraColonia, _coluna, _linha);
    _jogo->msg->Sucesso(("\tO castelo da colonia '" + _letraColonia + "' ira agora aparecer na posicao (" + Mensagem::IntToString(_linha) + "," + Mensagem::IntToString(_coluna) + ") do mapa.\n"));
}
