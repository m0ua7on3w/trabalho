//
// Created by andre on 13/12/2016.
//

#include "inicio.h"
#include "../../Game.h"
#include "../../Mensagem.h"
#include "../../Colonia.h"

bool inicio::VerificarArgs(stringstream &stringstream) {
    if(_jogo->_numJogadores <=0)
    {
        _jogo->msg->Erro(("ERRO! O comando OPONENTES ainda nao foi configurado!\n"));
        return false;
    }

    if(_jogo->_mLinhas == 0 || _jogo->_mColunas ==0)
    {
        _jogo->msg->Erro(("ERRO! O comando DIM ainda nao foi configurado!\n"));
        return false;
    }

    if(_jogo->_perfisJogador.size() == 0)
    {
        _jogo->msg->Erro(("ERRO! O comando MKPERFIL ainda nao foi configurado!\n"));
        return false;
    }
    if(_jogo->_moedasIniciais== 0)
    {
        _jogo->msg->Erro(("ERRO! O comando MOEDAS ainda nao foi configurado!\n"));
        return false;
    }
    if(_jogo->_perfisJogador.size() < 5)
    {
        _jogo->msg->Erro(("ERRO! Tem de criar 5 perfis inicialmente!\n"));
        return false;
    }

    return true;
}

void inicio::Aplicar()
{
    //_jogo->msg->Sucesso(("\n\tA iniciar jogo...\n"));
    //sleep(2000);
}

inicio::inicio(Game *jogo) {
    _jogo = jogo;
    _nome = "inicio";
    _mudarJogador=true;
}
