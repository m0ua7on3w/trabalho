//
// Created by andre on 13/12/2016.
//

#include <assert.h>
#include "oponentes.h"
#include "../../Game.h"
#include "../../Mensagem.h"

oponentes::oponentes(Game *jogo)
{
    _jogo = jogo;
    _nome = "oponentes";
}

bool oponentes::VerificarArgs(stringstream &stringstream)
{
    int n;

    if(_jogo->_mLinhas == 0 || _jogo->_mColunas == 0)
    {
        _jogo->msg->Erro(("ERRO! O comando DIM ainda nao foi configurado!\n"));
        return false;
    }

    if(!(stringstream >> n))
        return false;

    if(n < 1 || n > 12)
    {
        _jogo->msg->Erro(("ERRO! O limite e de 12 oponentes!\n"));
        return false;

    }

    _numOpo = n;
    return true;

}

char NumeroParaLetraVer2(int n)
{
    assert(n >= 1 && n <= 26);
    return "abcdefghijklmnopqrstuvwxyz"[n-1];
}

void oponentes::Aplicar() {
    _jogo->LimparCasteloEPosicoes();
    _jogo->_numJogadores = _numOpo+1;

    for(int i=0; i<_jogo->_numJogadores; i++)
    {
        stringstream ss;
        string letra;
        ss << NumeroParaLetraVer2(i+1);
        if(ss >> letra)
            _jogo->AdicionarCasteloEPosicoes(letra);

        ss.clear();
    }
}