//
// Created by andre on 14/12/2016.
//

#ifndef TRABALHO_LOADFILE_H
#define TRABALHO_LOADFILE_H


#include "../../Comando.h"

class loadFile : public Comando{
    string _nomeFicheiro;

public:
    loadFile(Game *jogo);
    virtual bool VerificarArgs(stringstream &stringstream);

    virtual void Aplicar();

};


#endif //TRABALHO_LOADFILE_H
