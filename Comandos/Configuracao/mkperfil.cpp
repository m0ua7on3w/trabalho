//
// Created by andre on 13/12/2016.
//

#include "mkperfil.h"
#include "../../Game.h"
#include <regex>
#include "../../Perfil.h"
#include "../../Mensagem.h"

bool mkperfil::VerificarArgs(stringstream &stringstream) {
    string letra;

    if(!(stringstream >> letra))
        return false;

    if(letra.length() != 1) return false;

    if(!std::regex_match(letra, regex("^[A-Za-z]+$"))) return false;

    bool existe = false;
    for(int i=0; i<_jogo->_perfisJogador.size(); i++)
    {
        //Verificar se o utilizador está a tentar criar um perfil ja existente
        if(_jogo->_perfisJogador[i]->ObterLetraPerfil() == letra)
            existe = true;
    }

    if(existe)
    {
        _jogo->msg->Erro("ERRO! Impossivel criar dois perfis com o mesmo nome.\n");
        return false;
    }

    _letraDada = letra;
    return true;
}

void mkperfil::Aplicar()
{
    _jogo->CriarPerfil(_letraDada);
}

mkperfil::mkperfil(Game *jogo) {
    _jogo = jogo;
    _nome = "mkperfil";
}
