//
// Created by andre on 11/12/2016.
//

#ifndef TRABALHO_PERFIL_H
#define TRABALHO_PERFIL_H

#include <iostream>
#include <vector>

class Caracteristica;
class Mapa;
class Ser;
using namespace std;

class Perfil
{
private:
    vector<Caracteristica*> _caracteristicas;
    int _maxForca = 10;
    int _custoTotal = 0;
    int _ataque=0, _defesa=0, _velocidade=0, _vida=0;
    string _letraPerfil;

public:

    Perfil(string letra);
    ~Perfil();

    bool AdicionarCaracteristica(Caracteristica *pCaracteristica);
    bool RemoverCaracteristica(int id);
    bool RemoverCaracteristica(Caracteristica *pCaracteristica);

    void SetSeres(Ser * ser);
    void Aplicar();
    void Reiniciar();
    void Atualizar(Mapa *m, Ser* ser);

    std::string ObterLetraPerfil();
    vector<int> ObterIndicesCaracteristicas();
    int getMaxForca(){ return _maxForca;};
    int getCustoTotal(){ return  _custoTotal;}
    bool CheckCaracteristicaExiste(int id);
    Caracteristica* CheckCaracteristicaExiste(int id,bool flag);

    void OrdenarCaracteristicas();
    void AdicionarValoresCumulativos(int ataque, int defesa, int velocidade,int vida);
    void LigarCarateristicas();

    friend std::ostream &operator<<(std::ostream &os, const Perfil *perfil);

};



#endif //TRABALHO_PERFIL_H
