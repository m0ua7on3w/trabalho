//
// Created by M0ua7on3w on 06/12/2016.
//

#include "Edificio.h"


void Edificio::Reparar()
{
    _vida = _vidaMaxima;
}

int Edificio::CustoReparacao() {
    if(_vida == _vidaMaxima)
        return 0;

    return _custoInicial-((_vida / _vidaMaxima)*(_custoInicial*0.8));
}
