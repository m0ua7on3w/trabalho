//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_EDIFICIO_H
#define TRABALHO_EDIFICIO_H

#include "../GameObject.h"
#include "iostream"
using namespace std;

class Edificio : public GameObject
{
protected:
    int _custoTotal = 0, _custoInicial=0;
public:
    virtual void Melhorar()=0;
    virtual int CustoReparacao();
    virtual void print(std::ostream &where) const=0;

    void Reparar();

    int getCustoTotal() const { return _custoTotal;}

    virtual void Atuar(Mapa *mapa)=0;
};


#endif //CASTLEWARZ_EDIFICIO_H
