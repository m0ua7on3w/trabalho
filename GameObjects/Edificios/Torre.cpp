//
// Created by M0ua7on3w on 06/12/2016.
//
#include "Torre.h"
#include "Castelo.h"
#include "../Ser.h"
#include "../../Mapa.h"
#include "../../Ponto.h"

void Torre::Atuar(Mapa *mapa) {
    Ser *outroSer = nullptr;
    int x,y;
    _pos->MeuXY(x,y);
    int xMax = (x+_range>=mapa->Colunas()) ? mapa->Colunas()-1 : x+_range;
    int yMax = (y+_range>=mapa->Linhas()) ? mapa->Linhas()-1 : y+_range;
    int xMin = (x-_range<0) ? 0 : x-_range;
    int yMin = (y-_range<0) ? 0 : y-_range;
    for(int i = xMin; i<xMax;i++){
        for (int j = yMin; j < yMax; j++) {
            outroSer = dynamic_cast<Ser*>( mapa->Obter(i,j));
            if(outroSer)
                if(ObterIdNacao() != outroSer->ObterIdNacao()){
                        outroSer->ReceberDano(_ataque);
                }
        }
    }

}

void Torre::Melhorar() {
    _nivel++;
    _defesa+=2;
    _ataque++;
    _custoTotal+=10;
}

Torre::Torre(Castelo *casa, Ponto *ponto) {
    _myID = EID++;
    _pos = ponto;
    _casa = casa;
    _vidaMaxima = 20;
    _vida = _vidaMaxima;
    _idNacao = _casa->ObterIdNacao();
    _nome = "T";
    _defesa=10;
    _custoInicial = 30;
    _custoTotal = 30;

}

void Torre::print(std::ostream &where) const
{
    int x,y;
    _pos->MeuXY(x,y);

    where << "\tNome: " << _nome
          << " | Posicao: " << "(" <<y<< "," <<x<< ")"
          << " | Defesa: " << _defesa
          << " | Saude: " << _vida
          << " | Ataque " << _ataque
          << " | Nivel: " << _nivel
          << " | EID: " << _myID
          << endl;

}


