//
// Created by M0ua7on3w on 06/12/2016.
//

#include "Quinta.h"
#include "../../Ponto.h"
#include "Castelo.h"
#include "../../Colonia.h"

Quinta::Quinta(Castelo *casa, Ponto *ponto, Colonia *colonia) {

    _minhaColonia = colonia;
    _myID=EID++;
    _pos = ponto;
    _casa = casa;
    _vidaMaxima = 20;
    _vida = _vidaMaxima;
    _idNacao = _casa->ObterIdNacao();
    _nome = "Q";
    _defesa=10;
    _custoInicial = 20;
    _custoTotal = 20;

}

void Quinta::Atuar(Mapa *mapa) {
    _minhaColonia->AumentarDinheiro(_moedasPorInstante);
}

void Quinta::Melhorar() {
    _nivel++;
    _defesa++;
    _moedasPorInstante++;
    _custoTotal+=10;
}

void Quinta::print(std::ostream &where) const {
    int x,y;
    _pos->MeuXY(x,y);

    where << "\tNome: " << _nome
          << " | Posicao: " << "(" <<y<< "," <<x<< ")"
          << " | Defesa: " << _defesa
          << " | Saude: " << _vida
          << " | Nivel: " << _nivel
          << " | Moedas por instante: " << _moedasPorInstante
          << " | EID: " << _myID
          << endl;
}
