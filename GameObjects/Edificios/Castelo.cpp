//
// Created by M0ua7on3w on 06/12/2016.
//

#include "Castelo.h"
#include "../Ser.h"
#include "../../Ponto.h"
#include "../../Mapa.h"


void Castelo::Atuar(Mapa *mapa) {
    for(int i = 0; i< _seresNoCastelo.size();i++){
            _seresNoCastelo[i]->ReceberDano(-1);
            _seresNoCastelo[i]->Reiniciar();
    }
}

Castelo::Castelo(Ponto* p, int idColonia, Mapa *mapa) {
    _vida=50;
    _myID = EID++;
    _pos=p;
    _defesa=10;
    _idNacao = idColonia;
    _nome = "C";
    _custoInicial = 50;
    _vidaMaxima=_vida;

    //load vizinhos

    int x,y;
    _pos->MeuXY(x,y);
    int xMax = (x+1>=mapa->Colunas()) ? x : x+1;
    int yMax = (y+1>=mapa->Linhas()) ? y : y+1;
    int xMin = (x-1<0) ? x : x-1;
    int yMin = (y-1<0) ? y : y-1;
    for(int i = xMin; i<= xMax; i++){
        for(int j = yMin; j<= yMax; j++){
            if(!(i==x && j== y)){
                _pontosVizinhos.push_back(new Ponto(i,j));
            }
        }
    }
}

bool Castelo::AdicionarSer(Ser *ser) {
    ser->ReceberDano(0);
    if(ser == nullptr)
        return false;
    _seresNoCastelo.push_back(ser);
    return true;
}

bool Castelo::RemoverSer(Ser *ser, Mapa *mapa) {

    for(int i = 0; i< _seresNoCastelo.size();i++){
        if(_seresNoCastelo[i]==ser){
            for(int j = 0; j< _pontosVizinhos.size();j++){
                int x,y;
                _pontosVizinhos[j]->MeuXY(x,y);
                if(mapa->Obter(x,y)== nullptr){
                    mapa->Adicionar(ser,x,y);
                    ser->ObterPosicao()->MudarXY(x,y);
                    _seresNoCastelo.erase(_seresNoCastelo.begin()+i);
                    return true;
                }
            }
        }
    }
    return false;
}

bool Castelo::Contem(Ser *ser) {
    for(int i = 0; i< _seresNoCastelo.size();i++){
        if(ser == _seresNoCastelo[i])
            return true;
    }
    return false;
}

void Castelo::print(std::ostream &where) const
{
    int x,y;
    _pos->MeuXY(x,y);

    where << "\tNome: " << _nome
          << " | Posicao: " << "(" <<y<< "," <<x<< ")"
          << " | Defesa: " << _defesa
          << " | Saude: " << _vida
            << " | Seres no castelo: " << _seresNoCastelo.size()
            << endl;
}

void Castelo::Melhorar() {
}

void Castelo::RecolherSeres(Mapa *mapa) {
    for(int j = 0; j< _pontosVizinhos.size();j++){
        int x,y;
        _pontosVizinhos[j]->MeuXY(x,y);
        Ser *s = dynamic_cast<Ser*>(mapa->Obter(x,y));
        if(s != nullptr){
            if(AdicionarSer(s)){
                mapa->Remover(s);
                s->ObterPosicao()->MudarXY(x,y);
            }
        }
    }
}

void Castelo::Mata(Mapa *mapa, Colonia *c) {
    mapa->Remover(this);
}

