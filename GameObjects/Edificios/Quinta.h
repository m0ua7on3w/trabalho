//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef CASTLEWARZ_QUINTA_H
#define CASTLEWARZ_QUINTA_H


#include "../Edificio.h"



class Quinta : public Edificio{
    Colonia *_minhaColonia;
    int _moedasPorInstante=2;
    int _nivel=1;
public:
    Quinta(Castelo *casa, Ponto *ponto, Colonia *colonia);
    virtual void Atuar(Mapa *mapa);
    virtual void Melhorar();
    int getNivel(){return _nivel;}


    virtual void print(std::ostream &where) const;
};


#endif //CASTLEWARZ_QUINTA_H
