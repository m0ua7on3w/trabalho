//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_CASTELO_H
#define TRABALHO_CASTELO_H

#include <vector>
#include "../Edificio.h"
class Ser;
class Mapa;


class Castelo : public Edificio {
    std::vector<Ser*> _seresNoCastelo;
    std::vector<Ponto*> _pontosVizinhos;

public:
    Castelo(Ponto *p, int idColonia, Mapa *mapa);
    virtual void Atuar(Mapa *mapa);

    virtual void print(std::ostream &where) const;

    virtual void Mata(Mapa *mapa, Colonia *c) override;

    virtual void Melhorar();
    void RecolherSeres(Mapa *mapa);
    bool AdicionarSer(Ser *ser);
    bool RemoverSer(Ser *ser,Mapa *mapa);
    bool Contem(Ser *ser);
};


#endif //CASTLEWARZ_CASTELO_H
