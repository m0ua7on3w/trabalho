//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef CASTLEWARZ_TORRE_H
#define CASTLEWARZ_TORRE_H


#include "../Edificio.h"

class Torre : public Edificio{
    int _range=2;
    int _ataque=3;
    int _nivel=1;
public:
    Torre(Castelo *casa, Ponto *ponto);
    virtual void Atuar(Mapa *mapa);
    virtual void Melhorar();
    int getNivel(){return _nivel;}

    virtual void print(std::ostream &where) const;
};


#endif //CASTLEWARZ_TORRE_H
