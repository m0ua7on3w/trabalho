//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_SER_H
#define TRABALHO_SER_H


#include "../GameObject.h"
#include "../Perfil.h"

class Castelo;
class Mapa;

class Ser : public GameObject{
    int _velocidade=0;
    int _ataque=0;
    Perfil *_perfil;
    bool _bandeira = false;
public:
    Ser(Castelo *casa ,Perfil *p, int id );
    void Atuar(Mapa *mapa);                     //Actualizar caracteristicas
    void MudarAtaque(int ataque);               //Set ataque
    void MudarDefesa(int defesa);               //Set defesa
    void MudarVidaMaxima(int vida);             //Set Vida Maxima
    void MudarVelocidade(int velocidade);       //Set Velocidade
    bool Mover(Ponto *p, Mapa *m);              //Mover o ser
    void Recuar(Mapa *mapa);                    //Recuar o ser - faz com que o ser se mova para casa/castelo
    void Reiniciar();                           //Reiniciar caracteristicas
    //void Atualizar(Mapa *m);  //isto está a mais? nunca é usado e o actuar ja chama o perfil->actualizar
    int getAtaque() const { return _ataque; }
    bool Paria(){return !_bandeira;};
    void JaFuiParia(){_bandeira = true;}

    ~Ser();

    virtual void print(std::ostream &where) const;
};


#endif //CASTLEWARZ_SER_H
