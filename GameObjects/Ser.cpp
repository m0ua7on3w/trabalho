//
// Created by M0ua7on3w on 06/12/2016.
//

#include <limits>
#include "Ser.h"
#include "../Ponto.h"
#include "../Mapa.h"
#include "Edificios/Castelo.h"
#include "../Caracteristica.h"
#include "../Caracteristicas/Bandeira.h"
#include "../Caracteristicas/Superior.h"
#include "../Caracteristicas/PeleDura.h"
#include "../Caracteristicas/Armadura.h"
#include "../Caracteristicas/Faca.h"
#include "../Caracteristicas/CaracteristicasLigadas/Espada.h"
#include "../Caracteristicas/Agressao.h"
#include "../Caracteristicas/Ecologico.h"
#include "../Caracteristicas/HeatSeeker.h"
#include "../Caracteristicas/BuildSeeker.h"
#include "../Caracteristicas/Walker.h"
#include "../Caracteristicas/Remedio.h"
#include "../Caracteristicas/SecondChance.h"
#include "../Caracteristicas/CaracteristicasLigadas/Aluno.h"
#include "../Colonia.h"

bool Ser::Mover(Ponto *p, Mapa *m) {

    int testx, testy;
    p->MeuXY(testx,testy);
    //O ser mexe-se sempre de forma autonoma e é perguicoso,
    //procura sempre o caminho mais perto
    int minDist = std::numeric_limits<int>::max();
    //Minha posicao ( _pos convertido para xy)
    int x,y;
    _pos->MeuXY(x,y);
    int MelhorEspaco[2]{x,y};
    int xMax = (x+1>=m->Colunas()) ? x : x+1;
    int yMax = (y+1>=m->Linhas()) ? y : y+1;
    int xMin = (x-1<0) ? 0 : x-1;
    int yMin = (y-1<0) ? 0 : y-1;
    for(int i = xMin; i<= xMax; i++){
        for(int j = yMin ;j<= yMax; j++){
            if(m->Obter(i,j) == nullptr){
                Ponto ponto(i,j);
                int DistanciaAtePonto = ponto.Distancia(p);
                if(DistanciaAtePonto<minDist ){
                    minDist = DistanciaAtePonto;
                    MelhorEspaco[0]=i;
                    MelhorEspaco[1]=j;
                }else if(DistanciaAtePonto==minDist){
                    Ponto tempH(MelhorEspaco[0],MelhorEspaco[1]);
                    float distAntiga = tempH.DistanciaHipotenusa(p);
                    float distNow = ponto.DistanciaHipotenusa(p);
                    if(distNow<distAntiga){
                        if(MelhorEspaco[0]!=x || MelhorEspaco [1]!=y){
                            MelhorEspaco[0]=i;
                            MelhorEspaco[1]=j;
                        }
                    }
                }
            }
        }
    }
    //Ja sei onde ir, MelhorEspaco.
    //Teleportacao 101 - Os basicos.
    //Posso ser removido do mapa?
    if(MelhorEspaco[0]!=x || MelhorEspaco [1]!=y){
        if (m->Remover(this)) {
            //Entao quero voltar a entrar! SuperPoder: On. brecha temporal
            bool pass = m->Adicionar(this, MelhorEspaco[0], MelhorEspaco[1]);
            if (pass) {
                _pos->MudarXY(MelhorEspaco[0], MelhorEspaco[1]);
                return true;
            }
        }
        return false;
    }

    return false;
}



void Ser::MudarDefesa(int defesa) {
    _defesa+=defesa;
}

void Ser::MudarAtaque(int ataque) {
    _ataque+=ataque;

}

void Ser::MudarVelocidade(int velocidade) {
    _velocidade+=velocidade;
}

void Ser::MudarVidaMaxima(int vida) {
    (_vidaMaxima-vida<=0)?_vidaMaxima=0:_vidaMaxima+=vida;
}

void Ser::Recuar(Mapa * mapa) {
    Mover(_casa->ObterPosicao(),mapa);
}

Ser::Ser(Castelo *casa, Perfil *p, int id) {
    _casa = casa;
    _myID = EID++;
    int x, y;
    _casa->ObterPosicao()->MeuXY(x,y);
    _pos = new Ponto(x,y);
    _casa->AdicionarSer(this);
    _vidaMaxima=10;
    _vida = _vidaMaxima;
    _idNacao = id;
    _perfil = new Perfil(p->ObterLetraPerfil());
    vector<int> a = p->ObterIndicesCaracteristicas();
    Caracteristica *c;
    c= nullptr;
    for(int i=0;i<a.size();i++){
        switch(a[i]){
            case 1:
                c = new Bandeira();break;
            case 2:
                c = new Superior();break;
            case 3:
                c = new PeleDura();break;
            case 4:
                c = new Armadura();break;
            case 5:
                c = new Faca();break;
            case 6:
                c = new Espada();break;
            case 7:
                c = new Agressao();break;
            case 8:
                c = new Ecologico();break;
            case 9:
                c = new HeatSeeker();break;
            case 10:
                c = new BuildSeeker();break;
            case 11:
                c = new Walker();break;
            case 12:
                c = new Remedio();break;
            case 13:
                c = new SecondChance();break;
            case 14:
                c = new Aluno();break;
            default:
                break;
        }
        if(c== nullptr)
            delete c;
        else
            _perfil->AdicionarCaracteristica(c);
    }
    _nome = _perfil->ObterLetraPerfil();
    _perfil->SetSeres(this);
    _perfil->Aplicar();
}


void Ser::Reiniciar() {
    _perfil->Reiniciar();
}

void Ser::Atuar(Mapa *mapa) {
    _perfil->Atualizar(mapa,this);
}

void Ser::print(std::ostream &where) const
{    int x,y;
    _pos->MeuXY(x,y);
    int cx, cy;
    std::string str("");
    _casa->ObterPosicao()->MeuXY(cx,cy);
    if(cx == x && cy == y){
        str+="Castelo ";
    }
    where << "\tPerfil do ser: '" << _nome << "'"
          << " | Posicao: " << str << "(" <<y<< "," <<x<< ")"
          << " | Saude: " << _vida
          << " | Ataque: " << _ataque
          << " | Defesa: " << _defesa
          << " | Velocidade: " << _velocidade
          << endl;
}

Ser::~Ser() {
    //Perfis
    delete _perfil;
}





