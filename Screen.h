//
// Created by M0ua7on3w on 14/12/2016.
//

#ifndef TRABALHO_SCREEN_H
#define TRABALHO_SCREEN_H


class Game;

class Screen {
protected:
    int _windowStartX;
    int _windowStartY;
public:
    virtual void Draw()=0;
    virtual void Clear()=0;
};


#endif //TRABALHO_SCREEN_H
