//
// Created by M0ua7on3w on 06/12/2016.
//

#include "Colonia.h"
#include "GameObjects/Ser.h"
#include "GameObjects/Edificios/Castelo.h"
#include "GameObjects/Edificios/Quinta.h"
#include "GameObjects/Edificios/Torre.h"
#include "Mapa.h"
#include "Ponto.h"
#include "Consola/Consola.h"

Ser *Colonia::CriarSer(int indicePerfil) {
    Perfil *p = _perfis[indicePerfil];
    Ser *ser=new Ser(_casa,p,_idColonia);
    _dinheiro -= p->getCustoTotal();
    _seres.push_back(ser);
    return ser;
}

Edificio *Colonia::CriarEdificio(Ponto *p, int tipo) {
    Edificio *ptr = nullptr;
    int x,y, preco=0;
    p->MeuXY(x,y);
    if(_mapa->Obter(x,y) != nullptr)
        return ptr;
    switch (tipo){
        case 1:
            ptr= new Torre(_casa,p);
            preco = 30;
            break;
        case 2:
            ptr=new Quinta(_casa,p,this);
            preco = 20;
            break;
        default:
            break;
    }
    if(ptr != nullptr)
    {
        _edificios.push_back(ptr);
        _mapa->Adicionar(ptr,x,y);
        _dinheiro -= preco;

    }
    return ptr;
}

void Colonia::AumentarDinheiro(int m) {
    _dinheiro+=m;
}

bool Colonia::EliminarSer(Ser *ser) {
    for(int i = 0; i< _seres.size();i++){
        if(_seres[i] == ser){
            _mapa->Remover(ser);
            delete _seres[i];
            _seres.erase(_seres.begin()+i);
            return true;
        }
    }
    return false;
}

bool Colonia::EliminarEdificio(Edificio *edificio) {
    for(int i = 0; i< _edificios.size();i++){
        if(_edificios[i] == edificio){
            _mapa->Remover(edificio);
            delete _edificios[i];
            _edificios.erase(_edificios.begin()+i);
            return true;
        }
    }
    return false;
}

bool Colonia::Eliminar(GameObject *go) {
    if(Ser *s = dynamic_cast<Ser*>(go)){
        EliminarSer(s);
        return true;
    }else if(Edificio *s = dynamic_cast<Edificio*>(go)){
        EliminarEdificio(s);
        return true;
    }else{
        return false;
    }
}

void Colonia::TentarMatar(Mapa *m) {
    for (int i = 0; i <_seres.size(); i++) {
        _seres[i]->Mata(m,this);
    }
    for (int i = 0; i < _edificios.size(); i++) {
        _edificios[i]->Mata(m,this);
    }

}

int Colonia::getDinheiro() const {
    return _dinheiro;
}

const string &Colonia::get_nome() const {
    return _nome;
}

ostream &operator<<(ostream &os, const Colonia *colonia)
{
    //Listar todas as inforcacoes desta colonia
    Consola::setTextColor(colonia->_idColonia+1);
    os << endl
       <<"\tInformacoes da Colonia: ";
    os << colonia->_nome;
    Consola::setTextColor(Consola::BRANCO);
    os << endl
       <<"\tDinheiro: " << colonia->_dinheiro << endl;
    //Listagem de Edificios
    os << endl << "\tLista de Edificios" << endl;

    for(int i=0; i<colonia->_edificios.size(); i++)
    {
        os << colonia->_edificios[i];
    }

    //Lista de Seres
    os << endl << "\tLista de Seres" << endl;
    if(colonia->_seres.size()==0)
        os << "\tLista vazia pois ainda nao foram adicionados seres!" << endl;
    else
    {
        os << "\tTotal de seres: " << colonia->_seres.size() << endl;
        for(int i =0; i<colonia->_seres.size(); i++)
        {
            os << colonia->_seres[i];
        }
    }
    return os;
}

void Colonia::Atuar() {

    for (int j = 0; j < _edificios.size(); j++) {
        _edificios[j]->Atuar(_mapa);
    }
    for (int i = 0; i < _seres.size(); i++) {
        if (_casa->Contem(_seres[i])) {
            _casa->RemoverSer(_seres[i], _mapa);
        } else {
            _seres[i]->Atuar(_mapa);
        }
    }
}

void Colonia::Recuar() {
    for(int i=0; i<_seres.size(); i++)
    {
        if(!_casa->Contem(_seres[i]))
            _seres[i]->Recuar(_mapa);
    }
    for(int j=0; j<_edificios.size(); j++)
    {
        _edificios[j]->Atuar(_mapa);
    }

    _casa->RecolherSeres(_mapa);
}

int Colonia::DistanciaDoCastelo(Ponto *edPos)
{
    Ponto *casaPos = _casa->ObterPosicao();
    int dist = edPos->Distancia(casaPos);
    return dist;
}

Edificio* Colonia::ObterEdificioPorEID(int eid) {
    for(int i = 0; i< _edificios.size(); i++){
        if(_edificios[i]->ObterEID()==eid){
            return _edificios[i];
        }
    }
    return nullptr;
}

Colonia::~Colonia() {
    /*std::vector<Edificio*> _edificios;
    std::vector<Ser*>_seres;*/
    for(int i=0; i<_seres.size(); i++)
    {
        _mapa->Remover(_seres[i]);
        delete _seres[i];
    }
    _seres.clear();
    for(int i=0; i<_edificios.size(); i++)
    {
        _mapa->Remover(_edificios[i]);
        delete _edificios[i];
    }
    _edificios.clear();
}

void Colonia::SetNext(int n) {
    _numeroDeNext=n;
}

bool Colonia::Perdeu() {
    int vida = _casa->getVida();
    return vida == 0;
}