//
// Created by M0ua7on3w on 14/12/2016.
//

#include <cstdlib>
#include <sstream>
#include "ScreenGame.h"
#include "Mapa.h"
#include "Consola/Consola.h"
#include "GameObject.h"
void ScreenGame::Draw() {

    Consola::clrscr();
    //Comeca a desenhar o mapa nos "pixeis" 0,0 ( default )
    Consola::gotoxy(_windowStartX,_windowStartY);
    //Onde comecar o for para linhas e colunas?
    //Por enquanto focus definido pelo comando
    int xStart = startingX;
    int yStart = startingY;


    //Onde acaba o for?
    //Se os valores estiverem corretos, i/j + largura/altura
    int xEnd = xStart+Largura;
    int yEnd = yStart+Altura;
    //Estes valores passam das linhas/colunas do mapa?
    if(xEnd>_mapa->Colunas()){ // Se passou das colunas, definir o maximo como colunas
        xEnd = _mapa->Colunas();
    }
    if(yEnd>_mapa->Linhas()){ // Se passou das linhas, definir o maximo como linhas
        yEnd = _mapa->Linhas();
    }

    //Para mostrar o numero da Coluna
    for(int i = xStart ;i <= xEnd;i++){
        if(i==xStart){
            std::cout << "  "<< (char)(179);
        }else{
            std::cout << Espacar(i-1) << (char)(179);
        }
    }
    std::cout << std::endl;
    //Comecar a imprimir as linhas
    for(int i = yStart; i<yEnd;i++){
        for(int j = xStart; j<=xEnd;j++){
            Consola::setTextColor(Consola::BRANCO);
            if(j==xStart){
                std::cout << Espacar(i) << (char)(179);
            }else{
                if(!_mapa->Obter(j-1,i)){
                    std::cout << (char)(179) << (char)(254) << (char)(179);
                }else{
                    Consola::setTextColor((_mapa->Obter(j-1,i)->ObterIdNacao()+1));
                    std::cout << " " <<_mapa->Obter(j-1,i)->ObterNome() << " ";
                }
            }
        }
        std::cout << std::endl;
    }
    Consola::setTextColor(Consola::BRANCO);
}

void ScreenGame::Clear() {
    system("cls");
}

ScreenGame::ScreenGame(Mapa *mapa) {
    _windowStartY = 0;
    _windowStartY = 0;
    _mapa=mapa;
}

std::string ScreenGame::Espacar(int n) {
    std::stringstream sstr;
    std::string str;
    if(n<10){
        sstr << " " << n;
    }else if(n>=10){
        sstr << n;
    }
    str=sstr.str();
    return str;
}

void ScreenGame::MudarFoco(int linha, int coluna) {
    startingX = coluna;
    startingY = linha;
}
