//
// Created by M0ua7on3w on 11/12/2016.
//

#include <algorithm>
#include <sstream>
#include "Jogador.h"
#include "../Input.h"
#include "../GameObjects/Edificios/Castelo.h"
#include "../Game.h"
#include "../Mapa.h"
#include "../Ponto.h"


bool Jogador::ObterInput() {
    if(Perdeu()){return true;}
    if(_numeroDeNext>0){
        string string1("ataca");
        stringstream sstr(string1);
        _numeroDeNext--;
        return _input->ObterInput(sstr);
    }
    string line,word;
    getline(cin,line);
    transform(line.begin(),line.end(),line.begin(),::tolower);
    stringstream ss(line);
    return _input->ObterInput(ss);
}

Jogador::Jogador(Game *jogo, string nome, int dinheiro, Ponto *casa, int id, vector<Perfil*> p) {
    _input = new Input(jogo);
    _nome = nome;
    _dinheiro = dinheiro;
    _idColonia=id;
    _edificios.push_back(new Castelo(casa, _idColonia,jogo->_mapa));
    _casa = (Castelo*)_edificios[0];
    int x,y;
    casa->MeuXY(x,y);
    _mapa = jogo->_mapa;
    _mapa->Adicionar(_casa,x,y);
    _perfis = p;
}

bool Jogador::ObterInput(std::stringstream &linha) {
    if(Perdeu()){return true;}
      return _input->ObterInput(linha);
}

Jogador::~Jogador() {
    delete _input;
}

