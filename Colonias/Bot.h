//
// Created by M0ua7on3w on 11/12/2016.
//

#ifndef TRABALHO_BOT_H
#define TRABALHO_BOT_H


#include "../Colonia.h"
#include <iostream>
#include <vector>
class Input;
class Game;
class Perfil;
class Ponto;
class Objetivos;


using namespace std;
class Bot : public Colonia{
    Input *_input;
    Objetivos *_obj;

    bool VerificarAumentoNivel();
    void CriarSeres();
    void CriarEdificios();
    void MelhorarEdificios();
    void PosicaoValidaParaEdificio(int &x, int &y);
    void AtacarOuRecuar();
public:
    Bot(Game *jogo, string nome, int dinheiro, Ponto *casa, int id, vector<Perfil*> p, Objetivos *objetivos);

    bool ObterInput();

    bool ObterInput(std::stringstream &linha) override;


};


#endif //TRABALHO_BOT_H
