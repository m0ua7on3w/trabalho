//
// Created by M0ua7on3w on 11/12/2016.
//

#include "Bot.h"
#include <algorithm>
#include <sstream>
#include "../Input.h"
#include "../GameObjects/Edificios/Castelo.h"
#include "../Game.h"
#include "../Mapa.h"
#include "../Ponto.h"
#include "../BotIA/Objetivos.h"
#include "../GameObjects/Edificios/Torre.h"
#include "../GameObjects/Edificios/Quinta.h"
#include "../GameObjects/Ser.h"
#include "../Perfil.h"

bool Bot::ObterInput() {
    if(Perdeu()){return true;}
    CriarEdificios();
    CriarSeres();
    MelhorarEdificios();
    if(VerificarAumentoNivel()){
        _obj->AumentarNivel();
    }
    _obj->AumentarTurno();
    AtacarOuRecuar();
    return true;
}

bool Bot::ObterInput(std::stringstream &linha) {
    return ObterInput();
}

Bot::Bot(Game *jogo, string nome, int dinheiro, Ponto *casa, int id, vector<Perfil*> p,Objetivos *objetivos){
    _input = new Input(jogo);
    _nome = nome;
    _dinheiro = dinheiro;
    _idColonia=id;
    _edificios.push_back(new Castelo(casa, _idColonia,jogo->_mapa));
    _casa = (Castelo*)_edificios[0];
    int x,y;
    casa->MeuXY(x,y);
    _mapa = jogo->_mapa;
    _mapa->Adicionar(_casa,x,y);
    int j[3]{-1};
    for(int i = 0;i<3;){
        int iNow = 0+rand()%5;
        if((iNow!=j[0]) && (iNow != j[1]) && (j[2]!=iNow)){
            i++;
            j[i]=iNow;
            _perfis.push_back(p[iNow]);
        }
    }
    _perfis.push_back(p[p.size()-2]);
    _perfis.push_back(p[p.size()-1]);
    _obj = objetivos;
}

bool Bot::VerificarAumentoNivel() {
    // Se os valores forem todos iguais, posso aumentar o nivel
    // O Ser nunca passara dos objetivos propostos

    //Os Meus seres em relacao ao Objetivo;
    int s = _seres.size();

    //Nivel e nulemero total das torres em relacao ao Objetivo
    int nT=0, numT=0;
    for(int i = 0; i<_edificios.size();i++){
        Torre *t = dynamic_cast<Torre*>(_edificios[i]);
        if(t){
            nT+=t->getNivel();
            numT++;
        }
    }
    if(numT>0){
        nT=(nT/numT);
    }

    //Nivel e numero total das torres em relacao ao Objetivo
    int nQ=0,numQ=0;
    for(int i = 0; i<_edificios.size();i++){
        Quinta *q = dynamic_cast<Quinta*>(_edificios[i]);
        if(q){
            nQ+=q->getNivel();
            numQ++;
        }
    }
    if(numQ>0){
        nQ=(nQ/numQ);
    }

    if(s!=(_obj->Seres()*5)){
        return false;
    }
    if(nT!=_obj->NivelTorres()){
        return false;
    }
    if(nQ!=_obj->NivelQuintas()){
        return false;
    }
    if(numT!=_obj->Torres()){
        return false;
    }
    if(numQ!=_obj->Quintas()){
        return false;
    }

    return true;
}

void Bot::CriarSeres() {
    for(int i = 0; i<_perfis.size(); i++){
        stringstream streamAEnviar;
        string nomePerfil = _perfis[i]->ObterLetraPerfil();
        int precoPerfil = _perfis[i]->getCustoTotal();
        int meusSeresComEstePerfil=0;
        int SeresACriar=0;
        int DinheiroTotalSeres;
        for(int j = 0;j<_seres.size();j++){
            string nomePerfilSer = _seres[j]->ObterNome();
            if(_seres[j]->ObterNome() == nomePerfil){
                meusSeresComEstePerfil++;
            }
        }
        SeresACriar = _obj->Seres() - meusSeresComEstePerfil;
        if(SeresACriar>0 && _dinheiro>precoPerfil){
            DinheiroTotalSeres=SeresACriar*precoPerfil;
            while(_dinheiro<DinheiroTotalSeres){
                SeresACriar--;
                DinheiroTotalSeres-=precoPerfil;
            }
            streamAEnviar << "ser " << SeresACriar << " " << nomePerfil;
            _input->ObterInput(streamAEnviar);
            streamAEnviar.clear();
        }
    }
}

void Bot::CriarEdificios() {
    int numQ=0;
    for(int i = 0; i<_edificios.size();i++){
        Quinta *q = dynamic_cast<Quinta*>(_edificios[i]);
        if(q){
            numQ++;
        }
    }

    stringstream streamAEnviar;
    int precoQuinta = 20;
    int quintasACriar = (_obj->Quintas() - numQ);
    if(quintasACriar>0)
        for(int i = 0; i < quintasACriar; i++){
            if(_dinheiro>precoQuinta){
                int x,y;
                PosicaoValidaParaEdificio(x,y);
                streamAEnviar << "build quinta " << y << " " << x;
                _input->ObterInput(streamAEnviar);
                streamAEnviar.clear();
            }
        }
    streamAEnviar.clear();
    int numT=0;
    for(int i = 0; i<_edificios.size();i++){
        Torre *t = dynamic_cast<Torre*>(_edificios[i]);
        if(t){
            numT++;
        }
    }

    int precoTorre = 30;
    int torresACriar = (_obj->Torres() - numT);
    if(torresACriar>0)
        for(int i = 0; i < torresACriar; i++){
            if(_dinheiro>precoTorre){
                int x,y;
                PosicaoValidaParaEdificio(x,y);
                streamAEnviar << "build torre " << y << " " << x;
                _input->ObterInput(streamAEnviar);
                streamAEnviar.clear();
            }
        }
}

void Bot::MelhorarEdificios() {
    stringstream streamAEnviar;
    int precoUpdate = 10;
    for(int i = 0; i < _edificios.size(); i++){
        if(_dinheiro>precoUpdate){
            Torre *t = dynamic_cast<Torre*>(_edificios[i]);
            if(t){
                if(t->getNivel()<_obj->NivelTorres()){
                    streamAEnviar << "upgrade " << t->ObterEID();
                    _input->ObterInput(streamAEnviar);
                    streamAEnviar.clear();
                }
            }
            Quinta *q = dynamic_cast<Quinta*>(_edificios[i]);
            if(q){
                if(q->getNivel()<_obj->NivelQuintas()){
                    streamAEnviar << "upgrade " << q->ObterEID();
                    _input->ObterInput(streamAEnviar);
                    streamAEnviar.clear();
                }
            }
        }
    }
}

void Bot::PosicaoValidaParaEdificio(int &x, int &y) {
    int CasteloX=0, CasteloY=0;
    _casa->ObterPosicao()->MeuXY(CasteloX,CasteloY);

    vector<Ponto> _posValidas;
    int xStart = CasteloX -10;
    if(xStart<0) xStart=0;
    int yStart = CasteloY - 10;
    if(yStart<0) yStart=0;
    int xEnd = CasteloX + 10;
    if(xEnd > _mapa->Colunas()-1) xEnd = _mapa->Colunas()-1;
    int yEnd = CasteloY + 10;
    if(yEnd > _mapa->Linhas()-1) yEnd = _mapa->Linhas()-1;

    for(int i = xStart; i<xEnd;i++){
        for(int j = yStart; j<yEnd;j++){
            if(_mapa->Obter(i,j)== nullptr){
                Ponto p(i,j);
                _posValidas.push_back(p);
            }
        }
    }
    if(_posValidas.size()>0)
        _posValidas[0+ rand() % _posValidas.size()].MeuXY(x,y);

}

void Bot::AtacarOuRecuar() {
    //Vida dos meus seres
    int vidaTotal=0;
    int vidaSeres=0;
    int cX, cY;
    for(int i =0; i<_seres.size();i++){
        if(_casa->ObterPosicao()->Distancia(_seres[i]->ObterPosicao())==0){
            vidaSeres+=_seres[i]->getVida();
            vidaTotal+=_seres[i]->getVidaMaxima();
        }
    }
    stringstream sstr;
    if(vidaTotal==0 || (vidaSeres*vidaTotal/100)>_obj->PorCentoRecolhe()){
        sstr<<"ataca";
        _input->ObterInput(sstr);
    }else{
        sstr<<"recolhe";
        _input->ObterInput(sstr);
    }
}

