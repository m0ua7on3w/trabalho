//
// Created by M0ua7on3w on 11/12/2016.
//

#ifndef TRABALHO_JOGADOR_H
#define TRABALHO_JOGADOR_H

#include "../Colonia.h"
class Input;
class Game;
class Perfil;
class Ponto;

#include <iostream>
#include <vector>

using namespace std;


class Jogador : public Colonia {
    Input *_input;
public:
    virtual ~Jogador();

    Jogador(Game *jogo, string nome,int dinheiro,Ponto *casa, int id, vector<Perfil*> p);
    virtual bool ObterInput();
    virtual bool ObterInput(std::stringstream &linha);
};


#endif //TRABALHO_JOGADOR_H
