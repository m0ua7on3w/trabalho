//
// Created by M0ua7on3w on 06/12/2016.
//

#include "Mapa.h"
#include "GameObject.h"
#include "Ponto.h"

Mapa::Mapa(int linhas, int colunas) {
    _linhas = linhas;
    _colunas = colunas;
    //Percore o mapa e mete o vector de game objects sem nada.
    for(int i = 0; i< linhas*colunas;i++)
    {
        _obj.push_back(nullptr);
    }
}

bool Mapa::Adicionar(GameObject *object, int x, int y) {
        if(x>=0 || y>=0 || x<=_colunas || y<=_linhas){
            if(Obter(x,y)== nullptr){
                _obj[y*_colunas+x] = object;
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
}

bool Mapa::Remover(GameObject *object) {
    if(object!= nullptr){
        int x,y;
        object->ObterPosicao()->MeuXY(x,y);
        _obj[y*_colunas+x] = nullptr;
        return true;
    }else{
        return false;
    }
}

GameObject *Mapa::Obter(int x, int y) {
    if(x>=0 || y>=0 || x<=_colunas || y<=_linhas)
        return _obj[y*_colunas+x];
    else
        return nullptr;
}

