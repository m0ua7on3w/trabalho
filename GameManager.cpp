//
// Created by M0ua7on3w on 22/01/2017.
//

#include <cstdlib>
#include "GameManager.h"
#include "ExitType.h"
#include "Game.h"
void GameManager::Start() {
    Menu();
}

void GameManager::NewGame() {
    Game *Jogo =new Game(_eType);
    while(Jogo->Configurar()){}
    Jogo->AplicarConfiguracao();
    _ACTIVEGAME = Jogo;
}

void GameManager::Save() {
    SaveGame s;
    s.jogo = _ACTIVEGAME;
    s._name = _eType->_exitArgs;
    _jogosDisponiveis.push_back(s);
    system("cls");
    _eType->_exitType = ExitType::NONE;
    NewGame();
}

void GameManager::Erase() {
    for(int i = 0; i < _jogosDisponiveis.size();i++){
        if(_jogosDisponiveis[i]._name == _eType->_exitArgs){
            if(_jogosDisponiveis[i].jogo==_ACTIVEGAME){
                _ACTIVEGAME= nullptr;
            }
            delete _jogosDisponiveis[i].jogo;
            _jogosDisponiveis.erase(_jogosDisponiveis.begin()+i);
            _eType->_exitType = ExitType::NONE;
        }
    }
}

void GameManager::Restore() {
    for(int i = 0; i < _jogosDisponiveis.size();i++){
        if(_jogosDisponiveis[i].jogo != nullptr){
            if(_jogosDisponiveis[i]._name == _eType->_exitArgs){
                _ACTIVEGAME = _jogosDisponiveis[i].jogo;
                _eType->_exitType = ExitType::NONE;
            }
        }
    }
}

void GameManager::Fim() {
    _ACTIVEGAME = NULL;
    delete _ACTIVEGAME;
    Break = true;
}

void GameManager::Win() {
    _ACTIVEGAME = NULL;
    delete _ACTIVEGAME;
    std::cout << "Ganhou!!" <<endl;
    Break = true;
}

void GameManager::Loss() {
    _ACTIVEGAME = NULL;
    delete _ACTIVEGAME;
    std::cout << "Perdeu!!" <<endl;
    Break = true;
}

void GameManager::Loop() {
    while(_eType = _ACTIVEGAME->Play()){
        switch (_eType->_exitType){
            case ExitType::WIN:
                Win();
                break;
            case ExitType::LOSS:
                Loss();
                break;
            case ExitType::ERASE:
                Erase();
                break;
            case ExitType::RESTORE:
                Restore();
                break;
            case ExitType::FIM:
                Fim();
                break;
            case ExitType::SAVE:
                Save();
                break;
            default:
                break;
        }
        if(_jogosDisponiveis.size()==0 && _ACTIVEGAME == nullptr){
            Break = true;
        }
        if(Break)
            break;
    }
}

GameManager::GameManager() {
    _eType = new ExitType(ExitType::NONE,"");
}

GameManager::~GameManager() {
    for(int i=0; i<_jogosDisponiveis.size();i++){
        delete _jogosDisponiveis[i].jogo;
    }
    delete _ACTIVEGAME;
    delete _eType;
}

void GameManager::Menu() {
    string choice;
    std::cout << "1 = Jogar" << std::endl;
    std::cout << "Esolha: ";
    getline(cin,choice);
    if(choice == "1"){
        NewGame();
        Loop();
    }
    std::cout <<std::endl << "O jogo acabou!";
    getline(cin,choice);

}
