//
// Created by andre on 16/01/2017.
//

#ifndef TRABALHO_MENSAGENS_H
#define TRABALHO_MENSAGENS_H


#include <iostream>

class Mensagem {

public:

    void Erro(std::string str);
    void Sucesso(std::string str);
    void Normal(std::ostringstream &oss);
    static std::string IntToString ( int number );
    void Info(std::string str);

};


#endif //TRABALHO_MENSAGENS_H
