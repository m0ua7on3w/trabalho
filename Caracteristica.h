//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_CARACTERISTICA_H
#define TRABALHO_CARACTERISTICA_H

#include <iostream>
class Mapa;
class Ser;
using namespace std;

class Caracteristica {
private:

protected:
    int _id;
    string _nome;
    int _custoMonetario;
    int _custoForca;
    Ser *_meuSer;
    int Ordem = 0;

public:
    virtual void Aplicar()=0;
    virtual void Atualizar(Mapa *p, Ser *ser)=0;
    virtual void Reiniciar()=0;
    void SetSer(Ser *s);
    int get_id() const;
    int get_Ordem() const { return Ordem;}
    const string &get_nome() const;
    int get_custoMonetario() const;
    int get_custoForca() const;

};


#endif //CASTLEWARZ_CARACTERISTICA_H
