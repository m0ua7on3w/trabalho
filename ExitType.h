//
// Created by M0ua7on3w on 21/01/2017.
//

#ifndef TRABALHO_EXITTYPE_H
#define TRABALHO_EXITTYPE_H

#include <iostream>

class ExitType {
public:
    enum type{
        NONE,
        FIM,
        SAVE,
        ERASE,
        RESTORE,
        WIN,
        LOSS
    };
    type _exitType;
    std::string _exitArgs;
    ExitType(type _exitType, const std::string &_exitArgs);
};


#endif //TRABALHO_EXITTYPE_H
