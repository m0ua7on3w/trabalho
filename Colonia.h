//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_COLONIA_H
#define TRABALHO_COLONIA_H

#include <iostream>
#include <vector>

class Castelo;
class Edificio;
class Ser;
class Perfil;
class GameObject;
class Mapa;
class Ponto;

class Colonia {
protected:
    Mapa *_mapa;
    std::vector<Edificio*> _edificios;
    std::vector<Ser*>_seres;
    Castelo *_casa;
    int _dinheiro =0;
    int _idColonia;
    std::string _nome;
    int _numeroDeNext=0;

    bool EliminarSer(Ser *ser);
    bool EliminarEdificio(Edificio *edificio);
public:
    std::vector<Perfil*> _perfis;
    virtual ~Colonia();

    void SetNext(int n);
    int getDinheiro() const;
    void AumentarDinheiro(int m);
    int DistanciaDoCastelo(Ponto *edPos);
    Edificio *ObterEdificioPorEID(int eid);
    const std::string &get_nome() const;

    virtual Ser *CriarSer(int indicePerfil);
    virtual Edificio *CriarEdificio(Ponto *p, int tipo);

    virtual bool Eliminar(GameObject *go);

    //Método overloaded para obter o input, através do utilizador ou através do ficheiro.
    //Metodos virtuais e putos
    virtual bool ObterInput()=0;
    virtual bool ObterInput(std::stringstream &linha)=0;

    void TentarMatar(Mapa *m);

    void Atuar();
    void Recuar();
    bool Perdeu();

    friend std::ostream &operator<<(std::ostream &os, const Colonia *colonia);

};

#endif //CASTLEWARZ_COLONIA_H
