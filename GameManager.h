//
// Created by M0ua7on3w on 22/01/2017.
//

#ifndef TRABALHO_GAMEMANAGER_H
#define TRABALHO_GAMEMANAGER_H


#include <vector>
#include <iostream>

class ExitType;
class Game;

class GameManager {
    class SaveGame{
    public:
        Game* jogo;
        std::string _name;
    };
    ExitType *_eType;
    std::vector<SaveGame> _jogosDisponiveis;
    Game *_ACTIVEGAME;

    bool Break = false;

public:
    GameManager();

    virtual ~GameManager();

private:
    void Win();
    void Loss();
    void Erase();
    void Restore();
    void Fim();
    void Save();
    void NewGame();
    void Loop();
    void Menu();
public:
    void Start();
};


#endif //TRABALHO_GAMEMANAGER_H
