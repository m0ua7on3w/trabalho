//
// Created by andre on 08/12/2016.
//

#ifndef TRABALHO_COMANDOS_H
#define TRABALHO_COMANDOS_H

#include <iostream>
#include <sstream>
using namespace std;
class Game;


class Comando {
protected:
    string _nome;
    Game *_jogo;
    bool _mudarJogador=false;

public:
    virtual bool VerificarArgs(stringstream &stringstream) = 0;
    virtual void Aplicar() = 0;
    bool MudarJogador(){return _mudarJogador; }

    string Nome(){ return _nome;}
};


#endif //TRABALHO_COMANDOS_H
