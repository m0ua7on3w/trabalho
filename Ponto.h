//
// Created by M0ua7on3w on 07/12/2016.
//

#ifndef CASTLEWARZ_PONTO_H
#define CASTLEWARZ_PONTO_H


class Ponto {
    int _x;
    int _y;
public:
    Ponto(int x, int y) : _x(x), _y(y){};
    void MudarXY(int x, int y);
    int Distancia(Ponto *p);
    float DistanciaHipotenusa(Ponto *p);
    void MeuXY(int &x, int &y);
};


#endif //CASTLEWARZ_PONTO_H
