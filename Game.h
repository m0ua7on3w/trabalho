//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_GAME_H
#define TRABALHO_GAME_H

#include <iostream>
#include <vector>
#include "BotIA/Objetivos.h"
#include "ExitType.h"

class Caracteristica;
class ScreenGame;
class Configuracao;
class Mapa;
class Colonia;
class Perfil;
class Mensagem;
using namespace std;


class Game {
    string _nomeJogoParaMemoria = "";
    ExitType *_exitType;
public:
    Game(ExitType *exitType);
    //Class para guardar as posicoes dos castelos
    class casteloPos{
    public:
        casteloPos(string letra, int l, int c):letra(letra), l(l), c(c){}
        string letra;
        int l,c;
    };

    vector<casteloPos*> c;
    ScreenGame *scrGame;
    Mensagem *msg;
    vector<Colonia*> _colonias;
    vector<Perfil*> _perfisJogador;
    vector<Perfil*> _perfisTotais;
    int _jogadorActual=0;
    int _moedasIniciais=0;
    int _mLinhas=0,_mColunas=0,_numJogadores=0;
    bool _jogando=true;
    Mapa *_mapa;
    Configuracao *_cfg;

    ~Game();

    bool Configurar();
    bool Configurar(stringstream &linha);
    ExitType *Play();
    bool Play(stringstream &linha);
    void AplicarConfiguracao();
    void CriarColonias();
    void CriarPerfil(string letra);
    bool ApagarPerfil(string letra);
    void AdicionarCasteloEPosicoes(string letra);
    void LimparCasteloEPosicoes();
    void MudarPosicaoCastelo(string letra, int x, int y);
    ExitType* CheckEnd();
    Perfil *PerfilRandom();
    Caracteristica *CriarCaracteristica(int id, Perfil *perfil);
    void SairJogo(ExitType::type eType, string str);

};


#endif //CASTLEWARZ_GAME_H
