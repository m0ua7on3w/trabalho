//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_SECONDCHANCE_H
#define TRABALHO_SECONDCHANCE_H


#include "../Caracteristica.h"


class SecondChance : public Caracteristica {
public:
    bool usado = false;
    SecondChance();
    virtual void Aplicar();

    virtual void Atualizar(Mapa *p, Ser *ser);

    virtual void Reiniciar();
};


#endif //TRABALHO_SECONDCHANCE_H
