//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_SUPERIOR_H
#define TRABALHO_SUPERIOR_H


#include "../Caracteristica.h"

class Superior : public Caracteristica{
public:
    Superior();
    virtual void Aplicar();

    virtual void Atualizar(Mapa *p, Ser *ser);

    virtual void Reiniciar();
};


#endif //TRABALHO_SUPERIOR_H
