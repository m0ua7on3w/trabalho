//
// Created by M0ua7on3w on 06/12/2016.
//

#include "PeleDura.h"
#include "../GameObjects/Ser.h"

void PeleDura::Aplicar()
{
    _meuSer->MudarDefesa(1);
}

void PeleDura::Reiniciar() {

}

PeleDura::PeleDura() {
    _id = 3;
    _nome = "Pele Dura";
    _custoMonetario = 2;
    _custoForca = 2;
    Ordem = 0;
}

void PeleDura::Atualizar(Mapa *p, Ser *ser) {

}
