//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_FACA_H
#define TRABALHO_FACA_H


#include "../Caracteristica.h"

class Faca :public Caracteristica {
public:
    Faca();
    virtual void Aplicar();

    virtual void Atualizar(Mapa *p, Ser *ser);

    virtual void Reiniciar();
};


#endif //TRABALHO_FACA_H
