//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_REMEDIO_H
#define TRABALHO_REMEDIO_H


#include "../Caracteristica.h"

class Remedio : public Caracteristica {
    int _stacks = 1;
public:
    Remedio();
    virtual void Aplicar() override;

    virtual void Atualizar(Mapa *p, Ser *ser);

    virtual void Reiniciar() override;
};


#endif //TRABALHO_REMEDIO_H
