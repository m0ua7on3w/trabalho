//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_BANDEIRA_H
#define TRABALHO_BANDEIRA_H


#include "../Caracteristica.h"

class Bandeira : public Caracteristica{
public:
    Bandeira();
    virtual void Aplicar();

    virtual void Atualizar(Mapa *p, Ser *ser);

    virtual void Reiniciar();
};


#endif //TRABALHO_BANDEIRA_H
