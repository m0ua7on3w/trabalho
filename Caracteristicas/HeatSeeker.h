//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_HEATSEEKER_H
#define TRABALHO_HEATSEEKER_H


#include "../Caracteristica.h"

class HeatSeeker : public Caracteristica
{

public:
    HeatSeeker();
    virtual void Aplicar();

    void Atualizar(Mapa *p, Ser *ser);

    virtual void Reiniciar();
};


#endif //TRABALHO_HEATSEEKER_H
