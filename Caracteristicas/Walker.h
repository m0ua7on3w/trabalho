//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_WALKER_H
#define TRABALHO_WALKER_H


#include "../Caracteristica.h"

class Walker : public Caracteristica {
public:
    Walker();
    virtual void Aplicar();

    virtual void Atualizar(Mapa *p, Ser *ser);

    virtual void Reiniciar();
};


#endif //TRABALHO_WALKER_H
