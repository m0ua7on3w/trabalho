//
// Created by M0ua7on3w on 06/12/2016.
//

#include "HeatSeeker.h"
#include <limits>
#include "../GameObjects/Ser.h"
#include "../Mapa.h"
#include "../Ponto.h"

void HeatSeeker::Aplicar() {
    _meuSer->MudarVelocidade(1);
}

void HeatSeeker::Reiniciar() {

}

HeatSeeker::HeatSeeker() {
    _id = 9;
    _nome = "HeatSeeker";
    _custoMonetario = 1;
    _custoForca = 1;
    Ordem = 20;

}

void HeatSeeker::Atualizar(Mapa *p, Ser *ser) {
    int dist = std::numeric_limits<int>::max();
    Ser *outroSer = nullptr;
    Ser *serAtacar = nullptr;
    for(int i = 0; i<p->Colunas();i++){
        for (int j = 0; j < p->Linhas(); j++) {
            outroSer = dynamic_cast<Ser*>( p->Obter(i,j));
            if(outroSer != nullptr && ser->ObterIdNacao() != outroSer->ObterIdNacao()) {
                int distGO = ser->ObterPosicao()->DistanciaHipotenusa(outroSer->ObterPosicao());
                if(distGO < dist){
                    dist = distGO;
                    serAtacar = outroSer;
                }
            }
        }
    }
    if(serAtacar!= nullptr)
        _meuSer->Mover(serAtacar->ObterPosicao(),p);
    else
        _meuSer->Recuar(p);

}
