//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_AGRESSAO_H
#define TRABALHO_AGRESSAO_H


#include <vector>
#include "../Caracteristica.h"

class CaracteristicaLigada;
class Agressao : public Caracteristica {
std::vector<CaracteristicaLigada*> _linkeds;
public:
    Agressao();
    virtual void Aplicar();

    void Atualizar(Mapa *p, Ser *ser) override;

    virtual void Reiniciar();

    void Ligar(CaracteristicaLigada * caract);
};


#endif //TRABALHO_AGRESSAO_H
