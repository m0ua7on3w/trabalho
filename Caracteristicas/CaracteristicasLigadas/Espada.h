//
// Created by andre on 13/12/2016.
//

#ifndef TRABALHO_ESPADA_H
#define TRABALHO_ESPADA_H


#include "../CaracteristicaLigada.h"

class Espada : public CaracteristicaLigada
{
int _stacks = 2;
public:
    Espada();
    virtual void Aplicar();

    virtual void Atualizar(Mapa *p, Ser *ser);

    virtual void Reiniciar();

    virtual void LinkedAtualizar();

};


#endif //TRABALHO_ESPADA_H
