//
// Created by M0ua7on3w on 06/12/2016.
//

#include "Aluno.h"
#include "../../Mapa.h"
#include "../../GameObjects/Ser.h"
#include "../../Ponto.h"

void Aluno::Aplicar() {


}

void Aluno::Reiniciar() {

}

Aluno::Aluno() {
    _id = 14;
    _nome = "Aluno";
    _custoMonetario = 1;
    _custoForca = 1;
    Ordem = 20;
}

void Aluno::Atualizar(Mapa *p, Ser *ser) {
    if(_vidaArmazenada<=0)
        return;
    Ser *outroSer = nullptr;
    int x,y;
    bool deuHeal=false;
    _meuSer->ObterPosicao()->MeuXY(x,y);
    int xMax = (x+1>=p->Colunas()) ? p->Colunas()-1 : x+1;
    int yMax = (y+1>=p->Linhas()) ? p->Linhas()-1 : y+1;
    int xMin = (x-1<0) ? 0 : x-1;
    int yMin = (y-1<0) ? 0 : y-1;
    for(int i = xMin; i<xMax;i++){
        for (int j = yMin; j < yMax; j++) {
            outroSer = dynamic_cast<Ser*>( p->Obter(i,j));
            if(outroSer)
                if(_meuSer->ObterIdNacao() == outroSer->ObterIdNacao()){
                    outroSer->ReceberDano(-_vidaArmazenada);
                    deuHeal=true;
                }
        }
    }
    if(deuHeal){
        _vidaArmazenada = 0;
    }
}

void Aluno::LinkedAtualizar() {
    _vidaArmazenada++;
}
