//
// Created by andre on 13/12/2016.
//

#include "Espada.h"
#include "../../GameObjects/Ser.h"
#include "../../Mapa.h"

Espada::Espada() {
    _id = 6;
    _nome = "Espada";
    _custoMonetario = 2;
    _custoForca = 2;
    Ordem = 20;
}

void Espada::Aplicar()
{
    _meuSer->MudarAtaque(3);
}

void Espada::Reiniciar()
{
    //Ataque foi reduzido
    if(_stacks==0){
        _meuSer->MudarAtaque(1);
        _stacks=2;
    }else if(_stacks == 1){ //Usou uma stacks, nao chegou ao fim o ataque nao reduziu
        _stacks = 2;
    }
}

void Espada::Atualizar(Mapa *p, Ser *ser)
{

}

void Espada::LinkedAtualizar() {
    if(_stacks>=1){
        _stacks--;
        if(_stacks==0){
            _meuSer->MudarAtaque(-1);
        }
    }
}

