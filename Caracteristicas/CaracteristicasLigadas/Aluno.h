//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_ALUNO_H
#define TRABALHO_ALUNO_H


#include "../CaracteristicaLigada.h"

class Aluno  : public CaracteristicaLigada {
    int _vidaArmazenada=0;
public:
    void LinkedAtualizar() override;

    Aluno();
    virtual void Aplicar();

    virtual void Atualizar(Mapa *p, Ser *ser);

    virtual void Reiniciar() ;
};


#endif //TRABALHO_ALUNO_H
