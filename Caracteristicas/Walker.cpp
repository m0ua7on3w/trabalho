//
// Created by M0ua7on3w on 06/12/2016.
//

#include <vector>
#include <cstdlib>
#include "Walker.h"
#include "../Ponto.h"
#include "../GameObjects/Ser.h"
#include "../Mapa.h"

void Walker::Aplicar() {

}

void Walker::Reiniciar() {

}

Walker::Walker() {
    _id = 11;
    _nome = "Walker";
    _custoMonetario = 1;
    _custoForca = 1;
    Ordem = 9;
}

void Walker::Atualizar(Mapa *p, Ser *ser) {
    int x, y;
    ser->ObterPosicao()->MeuXY(x,y);
    std::vector<Ponto> _posValidas;
    int xStart = x -1;
    if(xStart<0) xStart=0;
    int yStart = y - 1;
    if(yStart<0) yStart=0;
    int xEnd = x + 1;
    if(xEnd > p->Colunas()-1) xEnd = p->Colunas()-1;
    int yEnd = y + 1;
    if(yEnd > p->Linhas()-1) yEnd = p->Linhas()-1;

    for(int i = xStart; i<xEnd;i++){
        for(int j = yStart; j<yEnd;j++){
            if(p->Obter(i,j)== nullptr){
                Ponto ponto(i,j);
                _posValidas.push_back(ponto);
            }
        }
    }
    if(_posValidas.size()>0){
        int x1,y1;
        _posValidas[0+ rand() % _posValidas.size()].MeuXY(x1,y1);
        if(x1!=x || y1!=y){
            if (p->Remover(_meuSer)) {
                //Entao quero voltar a entrar! SuperPoder: On. brecha temporal
                bool pass = p->Adicionar(_meuSer, x1, y1);
                if (pass) {
                    _meuSer->ObterPosicao()->MudarXY(x1, y1);
                }
            }
        }
    }
}
