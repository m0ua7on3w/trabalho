//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_ECOLOGICO_H
#define TRABALHO_ECOLOGICO_H


#include <vector>
#include "../Caracteristica.h"
class CaracteristicaLigada;

class Ecologico : public Caracteristica {
    std::vector<CaracteristicaLigada *> _linkeds;
public:
    Ecologico();
    virtual void Aplicar();

    virtual void Atualizar(Mapa *p, Ser *ser);

    virtual void Reiniciar();

    void Ligar(CaracteristicaLigada * caract);
};


#endif //TRABALHO_ECOLOGICO_H
