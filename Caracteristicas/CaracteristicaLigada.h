//
// Created by M0ua7on3w on 20/01/2017.
//

#ifndef TRABALHO_CARACTERISTICASLIGADAS_H
#define TRABALHO_CARACTERISTICASLIGADAS_H


#include "../Caracteristica.h"

class CaracteristicaLigada : public Caracteristica {
public:
    virtual void LinkedAtualizar()=0;
};


#endif //TRABALHO_CARACTERISTICASLIGADAS_H
