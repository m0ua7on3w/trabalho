//
// Created by M0ua7on3w on 06/12/2016.
//

#include "Remedio.h"
#include "../GameObjects/Ser.h"

void Remedio::Aplicar() {

}


void Remedio::Reiniciar() {
    _stacks=1;
}

Remedio::Remedio() {
    _id = 12;
    _nome = "Remedio";
    _custoMonetario = 2;
    _custoForca = 1;
    Ordem = 0;
}

void Remedio::Atualizar(Mapa *p, Ser *ser) {
    if(_stacks>0){
        if(_meuSer->getVida()<=3){
            _meuSer->ReceberDano(-2);
            _stacks=0;
        }
    }
}
