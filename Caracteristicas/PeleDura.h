//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_PELEDURA_H
#define TRABALHO_PELEDURA_H


#include "../Caracteristica.h"

class PeleDura : public Caracteristica {
public:
    PeleDura();
    virtual void Aplicar() ;

    virtual void Atualizar(Mapa *p, Ser *ser);

    virtual void Reiniciar();
};


#endif //TRABALHO_PELEDURA_H
