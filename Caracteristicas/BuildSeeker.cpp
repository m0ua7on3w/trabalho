//
// Created by M0ua7on3w on 06/12/2016.
//

#include <limits>
#include "BuildSeeker.h"
#include "../Mapa.h"
#include "../GameObjects/Ser.h"
#include "../GameObjects/Edificio.h"
#include "../Ponto.h"

void BuildSeeker::Aplicar() {
   _meuSer->MudarVelocidade(1);
}

void BuildSeeker::Reiniciar() {

}

BuildSeeker::BuildSeeker() {
    _id = 10;
    _nome = "BuildSeeker";
    _custoMonetario = 1;
    _custoForca = 1;
    Ordem = 10;
}

void BuildSeeker::Atualizar(Mapa *p, Ser *ser) {
    int dist = std::numeric_limits<int>::max();
    Edificio *outroEdificio = nullptr;
    Edificio *EdificioAtacar = nullptr;
    for(int i = 0; i<p->Colunas();i++){
        for (int j = 0; j < p->Linhas(); j++) {
            outroEdificio = dynamic_cast<Edificio*>(p->Obter(i,j));
            if(outroEdificio != nullptr && ser->ObterIdNacao() != outroEdificio->ObterIdNacao()) {
                int distGO = ser->ObterPosicao()->DistanciaHipotenusa(outroEdificio->ObterPosicao());
                if(distGO < dist){
                    dist = distGO;
                    EdificioAtacar = outroEdificio;
                }
            }
        }
    }
    if(EdificioAtacar != nullptr)
        _meuSer->Mover(EdificioAtacar->ObterPosicao(),p);
    else
        _meuSer->Recuar(p);
}
