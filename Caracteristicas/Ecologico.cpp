//
// Created by M0ua7on3w on 06/12/2016.
//

#include "Ecologico.h"
#include "../GameObjects/Ser.h"
#include "../Ponto.h"
#include "../Mapa.h"
#include "../GameObjects/Edificio.h"
#include "CaracteristicasLigadas/Espada.h"

void Ecologico::Aplicar()
{

}

void Ecologico::Reiniciar() {

}

Ecologico::Ecologico() {
    _id = 8;
    _nome = "Ecologico";
    _custoMonetario = 1;
    _custoForca = 1;
    Ordem = 30;
}

void Ecologico::Atualizar(Mapa *p, Ser *ser) {
    if(_meuSer->Paria()){
        return;
    }
    Edificio *outroEdificio = nullptr;
    int x,y;
    _meuSer->ObterPosicao()->MeuXY(x,y);
    int xMax = (x+1>=p->Colunas()) ? p->Colunas()-1 : x+1;
    int yMax = (y+1>=p->Linhas()) ? p->Linhas()-1 : y+1;
    int xMin = (x-1<0) ? 0 : x-1;
    int yMin = (y-1<0) ? 0 : y-1;
    for(int i = xMin; i<xMax;i++){
        for (int j = yMin; j < yMax; j++) {
            outroEdificio = dynamic_cast<Edificio*>( p->Obter(i,j));
            if(outroEdificio)
                if(_meuSer->ObterIdNacao() != outroEdificio->ObterIdNacao()){
                    int dano = _meuSer->getAtaque();
                    if(dano>=0)
                        outroEdificio->ReceberDano(dano);
                    else{
                        _meuSer->ReceberDano(1);
                    }
                    for(int i =0;i<_linkeds.size();i++){
                        _linkeds[i]->LinkedAtualizar();
                    }
                    return;
                }
        }
    }
}

void Ecologico::Ligar(CaracteristicaLigada *caract) {
    _linkeds.push_back(caract);
}
