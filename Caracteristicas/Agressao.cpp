//
// Created by M0ua7on3w on 06/12/2016.
//

#include "Agressao.h"
#include "../Mapa.h"
#include "../GameObjects/Ser.h"
#include "../Ponto.h"
#include "CaracteristicasLigadas/Espada.h"
void Agressao::Aplicar()
{

}

void Agressao::Reiniciar()
{

}

Agressao::Agressao() {
    _id = 7;
    _nome = "Agressao";
    _custoMonetario = 1;
    _custoForca = 1;
    Ordem = 30;

}

void Agressao::Atualizar(Mapa *p, Ser *ser)
{
    if(_meuSer->Paria()){
        return;
    }
    Ser *outroSer = nullptr;
    int x,y;
    _meuSer->ObterPosicao()->MeuXY(x,y);
    int xMax = (x+1>=p->Colunas()) ? p->Colunas()-1 : x+1;
    int yMax = (y+1>=p->Linhas()) ? p->Linhas()-1 : y+1;
    int xMin = (x-1<0) ? 0 : x-1;
    int yMin = (y-1<0) ? 0 : y-1;
    for(int i = xMin; i<xMax;i++){
        for (int j = yMin; j < yMax; j++) {
            outroSer = dynamic_cast<Ser*>( p->Obter(i,j));
            if(outroSer)
                if(_meuSer->ObterIdNacao() != outroSer->ObterIdNacao()){
                    int dano = _meuSer->getAtaque() - outroSer->getDefesa();
                    if(dano>=0)
                        outroSer->ReceberDano(dano);
                    else{
                        _meuSer->ReceberDano(1);
                    }
                    for(int i =0;i<_linkeds.size();i++){
                        _linkeds[i]->LinkedAtualizar();
                    }
                    return;
                }
        }
    }
}

void Agressao::Ligar(CaracteristicaLigada *caract) {
    _linkeds.push_back(caract);
}
