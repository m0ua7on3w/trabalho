//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_BUILDSEEKER_H
#define TRABALHO_BUILDSEEKER_H


#include "../Caracteristica.h"
class mapa;

class BuildSeeker :public Caracteristica {
    int _distancia;
public:
    BuildSeeker();
    virtual void Aplicar();

    virtual void Atualizar(Mapa *p, Ser *ser);

    virtual void Reiniciar();
};


#endif //CASTLEWARZ_BUILDSEEKER_H
