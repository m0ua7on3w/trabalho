//
// Created by M0ua7on3w on 06/12/2016.
//

#ifndef TRABALHO_ARMADURA_H
#define TRABALHO_ARMADURA_H


#include "../Caracteristica.h"

class Armadura : public Caracteristica {
public:
    Armadura();
    virtual void Aplicar();

    virtual void Atualizar(Mapa *p, Ser *ser);

    virtual void Reiniciar() ;
};


#endif //TRABALHO_ARMADURA_H
