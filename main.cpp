
#include <time.h>
#include <ctime>
#include <cstdlib>
#include "Game.h"
#include "ScreenGame.h"
#include "Consola/Consola.h"
#include "ExitType.h"
#include "GameManager.h"

int main() {
    std::srand(static_cast<unsigned int>(std::time(NULL)));
    GameManager GM;
    GM.Start();



    return 0;
}