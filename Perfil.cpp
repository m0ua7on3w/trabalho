
// Created by andre on 11/12/2016.
//

#include <algorithm>
#include "Perfil.h"
#include "Caracteristica.h"
#include "Caracteristicas/Agressao.h"
#include "Caracteristicas/CaracteristicasLigadas/Espada.h"
#include "Caracteristicas/Ecologico.h"
#include "Caracteristicas/CaracteristicaLigada.h"

Perfil::Perfil(string letra)
{
    _letraPerfil = letra;
}

bool Perfil::AdicionarCaracteristica(Caracteristica *pCaracteristica)
{
    int forcaDisponivel = _maxForca-pCaracteristica->get_custoForca();
    if(forcaDisponivel<0 || pCaracteristica == nullptr)
        return false;
    _maxForca-=pCaracteristica->get_custoForca();
    _custoTotal+=pCaracteristica->get_custoMonetario();
    _caracteristicas.push_back(pCaracteristica);
    OrdenarCaracteristicas();
    return true;
}

bool Perfil::RemoverCaracteristica(int id) {
    for(int i = 0; i< _caracteristicas.size();i++)
    {
        if(_caracteristicas[i]->get_id()==id){
            delete _caracteristicas[i];
            _caracteristicas.erase(_caracteristicas.begin()+i);
            return true;
        }
    }
    return false;
}

void Perfil::Aplicar() {
    for(int i=0; i< _caracteristicas.size();i++)
    {
        _caracteristicas[i]->Aplicar();
    }
    LigarCarateristicas();
}

void Perfil::Atualizar(Mapa *m, Ser* ser) {
    for(int i=0; i< _caracteristicas.size();i++){
        _caracteristicas[i]->Atualizar(m,ser);
    }
}

void Perfil::Reiniciar() {
    for(int i=0; i< _caracteristicas.size();i++){
        _caracteristicas[i]->Reiniciar();
    }
}

std::string Perfil::ObterLetraPerfil() {
    return _letraPerfil;
}

void Perfil::SetSeres(Ser *ser) {
    for (int i = 0; i < _caracteristicas.size(); i++)
    {
        _caracteristicas[i]->SetSer(ser);
    }
}

bool Perfil::CheckCaracteristicaExiste(int id) {
    for(int i = 0; i< _caracteristicas.size();i++)
    {
        if(_caracteristicas[i]->get_id()==id)
        {
            return true;
        }
    }
    return false;
}
Caracteristica* Perfil::CheckCaracteristicaExiste(int id, bool flag) {
    for(int i = 0; i< _caracteristicas.size();i++)
    {
        if(_caracteristicas[i]->get_id()==id)
        {
            return _caracteristicas[i];
        }
    }
    return nullptr;
}

vector<int> Perfil::ObterIndicesCaracteristicas() {
    vector<int> toReturn;
    for(int i =0; i< _caracteristicas.size();i++){
        toReturn.push_back(_caracteristicas[i]->get_id());
    }
    return toReturn;
}

std::ostream &operator<<(std::ostream &os, const Perfil *perfil)
{
    os << "\tPerfil '" << perfil->_letraPerfil << "'" << endl << "\tCusto do perfil: " <<perfil->_custoTotal << endl;
    for (int i = 0; i < perfil->_caracteristicas.size(); i++)
    {
        os << "\tCaracteristica " << i+1 << ": " << perfil->_caracteristicas[i]->get_nome() << endl;
    }
    os<< endl << "\tValores cumulativos" << endl;
    os << "\tAtaque: " << perfil->_ataque << " | Defesa: " << perfil->_defesa << " | Velocidade: " << perfil->_velocidade << " | Vida: " << perfil->_vida << endl;
    return os;
}

void Perfil::OrdenarCaracteristicas() {
    for( int i = 0; i<_caracteristicas.size(); i++){
        for( int j = 0; j<_caracteristicas.size()-1; j++){
            int o1=_caracteristicas[j]->get_Ordem();
            int o2=_caracteristicas[j+1]->get_Ordem();
            if(o1>o2){
                Caracteristica *temp = _caracteristicas[j+1];
                _caracteristicas[j+1] = _caracteristicas[j];
                _caracteristicas[j]=temp;
            }
        }
    }
}

void Perfil::AdicionarValoresCumulativos(int ataque, int defesa, int velocidade, int vida)
{
    _ataque += ataque;
    _defesa += defesa;
    _velocidade += velocidade;
    _vida += vida;

}

Perfil::~Perfil() {
    //Caracteristicas
    for(int i = 0; i<_caracteristicas.size();i++){
        delete _caracteristicas[i];
    }
    _caracteristicas.clear();
}

void Perfil::LigarCarateristicas() {
    //Ligar espada a Agressao e Ecologico;
    for(int i=0; i<_caracteristicas.size();i++){
        if(_caracteristicas[i]->get_id()==6 || _caracteristicas[i]->get_id()==14){ // ID espada ou Aluno
            CaracteristicaLigada *linked = dynamic_cast<CaracteristicaLigada*>(_caracteristicas[i]);
            if(linked){
                for(int j=0; j<_caracteristicas.size();j++){
                    if(_caracteristicas[j]->get_id()==7){ // Agressao
                        Agressao *tmp = dynamic_cast<Agressao*>(_caracteristicas[j]);
                        if(tmp){
                            tmp->Ligar(linked);
                        }
                    }else if(_caracteristicas[j]->get_id()==8){ // Ecologico
                        Ecologico *tmp = dynamic_cast<Ecologico*>(_caracteristicas[j]);
                        if(tmp){
                            tmp->Ligar(linked);
                        }
                    }
                }
            }
        }
    }
}

bool Perfil::RemoverCaracteristica(Caracteristica *pCaracteristica) {
    for(int i = 0; i< _caracteristicas.size();i++)
    {
        if(_caracteristicas[i] == pCaracteristica){
            delete _caracteristicas[i];
            _caracteristicas.erase(_caracteristicas.begin()+i);
            return true;
        }
    }
    return false;
}



