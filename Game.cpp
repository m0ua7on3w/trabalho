//
// Created by M0ua7on3w on 06/12/2016.
//

#include <algorithm>
#include <sstream>
#include <assert.h>
#include "Game.h"

#include "Mapa.h"
#include "Colonia.h"
#include "Colonias/Jogador.h"
#include "Perfil.h"
#include "Ponto.h"
#include "cstdlib"
#include "Configuracao.h"
#include "ScreenGame.h"
#include "Mensagem.h"
#include "Consola/Consola.h"
#include "Colonias/Bot.h"
#include "BotIA/Objetivos.h"
#include "Caracteristicas/CaracteristicasLigadas/Aluno.h"
#include "Caracteristicas/SecondChance.h"
#include "Caracteristicas/Remedio.h"
#include "Caracteristicas/Walker.h"
#include "Caracteristicas/BuildSeeker.h"
#include "Caracteristicas/HeatSeeker.h"
#include "Caracteristicas/Ecologico.h"
#include "Caracteristicas/Agressao.h"
#include "Caracteristicas/CaracteristicasLigadas/Espada.h"
#include "Caracteristicas/Faca.h"
#include "Caracteristicas/Armadura.h"
#include "Caracteristicas/PeleDura.h"
#include "Caracteristicas/Bandeira.h"
#include "Caracteristicas/Superior.h"
#include "ExitType.h"

bool Game::Configurar() {
    string line,word;
    std::cout << endl << "Introduza o comando: ";
    getline(cin,line);
    transform(line.begin(),line.end(),line.begin(),::tolower);

    stringstream ss(line);

    return _cfg->ObterInput(ss);
}

bool Game::Configurar(stringstream &linha) {
    std::cout << endl << "Lido por ficheiro: " << linha.str() <<endl;

    return _cfg->ObterInput(linha);
}

ExitType* Game::Play() {
    if(_jogadorActual==0)
        scrGame->Draw();

    do{
        Consola::setTextColor(_jogadorActual+1);
        std::cout << endl << "Jogador " << _colonias[_jogadorActual]->get_nome();
        Consola::setTextColor(Consola::BRANCO);
        std::cout << ": ";
    }while(!_colonias[_jogadorActual]->ObterInput());

    for(int j=0; j< _colonias.size();j++){
        _colonias[j]->TentarMatar(_mapa);
    }
    if(_exitType->_exitType==ExitType::NONE){
        _jogadorActual++;
        if(_jogadorActual>=_colonias.size())_jogadorActual=0;
    }

    return CheckEnd();
}

bool Game::Play(stringstream &linha) {
    if(_jogadorActual==0)
        scrGame->Draw();
    string str = linha.str();
    Consola::setTextColor(_jogadorActual+1);
    std::cout << endl << "Jogador " << _colonias[_jogadorActual]->get_nome();
    Consola::setTextColor(Consola::BRANCO);
    std::cout << ": " << str << endl;
    bool test = _colonias[_jogadorActual]->ObterInput(linha);
    if (test)
    {
       _jogadorActual++;

    }

    if (_jogadorActual == _colonias.size()) {
        _jogadorActual = 0;
    }
    for (int j = 0; j < _colonias.size(); j++) {
        _colonias[j]->TentarMatar(_mapa);
    }

    if (_jogando == false)
        return false;


    return true;
}

void Game::CriarColonias() {
    for(int i = 0; i<_perfisJogador.size();i++){
        _perfisTotais.push_back(_perfisJogador[i]);
    }
    _colonias.push_back(new Jogador(this, c[0]->letra,_moedasIniciais,new Ponto(c[0]->c,c[0]->l),0,_perfisJogador));
    for(int i = 1; i<_numJogadores;i++){
        _perfisTotais.push_back(PerfilRandom());
        _perfisTotais.push_back(PerfilRandom());
        _colonias.push_back(new Bot(this
                ,c[i]->letra,_moedasIniciais
                ,new Ponto(c[i]->c,c[i]->l)
                ,i,_perfisTotais
                ,new Objetivos((0 + rand() % 2)+1,static_cast<Objetivos::TiposIA >(0 + rand() % 2),static_cast<Objetivos::TipoSecundario >(0 + rand() % 3))));
    }
}

void Game::CriarPerfil(string letra) {
    _perfisJogador.push_back(new Perfil(letra));
}

bool Game::ApagarPerfil(string letra) {
    for(int i = 0; i< _perfisJogador.size();i++){
        if(_perfisJogador[i]->ObterLetraPerfil()==letra)
        {
            delete _perfisJogador[i];
            _perfisJogador.erase(_perfisJogador.begin()+i);
            return true;
        }
    }
    return false;
}

int random(int min, int max)
{
    return min + rand() % max;
}

void Game::AdicionarCasteloEPosicoes(string letra)
{
    //Ponto p(_mLinhas,_mColunas);
    int randomLinha = random(0, _mLinhas);
    int randomColuna = random(0, _mColunas);

    c.push_back(new casteloPos(letra,randomLinha,randomColuna));
}

void Game::LimparCasteloEPosicoes() {
    c.erase(c.begin(),c.end());
}

void Game::MudarPosicaoCastelo(string letra, int x, int y) {
    for(int i = 0; i< c.size();i++)
    {
        if(c[i]->letra == letra){
            c[i]->l = y;
            c[i]->c = x;
        }
    }
}

void Game::AplicarConfiguracao() {
    _mapa = new Mapa(_mLinhas, _mColunas);
    CriarColonias();
    scrGame = new ScreenGame(_mapa);
}

Game::~Game() {

    //Castelo Pos
    for(int i = 0; i<c.size();i++){
        delete c[i];
    }
    c.clear();

    //Screen Game
    delete scrGame;

    //Mensagem
    delete msg;

    //Colonias
    for(int i = 0; i<_colonias.size();i++){
        delete _colonias[i];
    }
    _colonias.clear();

    //Perfis
    for(int i = 0; i<_perfisJogador.size();i++){
        delete _perfisJogador[i];
    }
    _perfisJogador.clear();

    //Mapa
    delete _mapa;

    //Configuracao
    delete _cfg;

    delete _exitType;
}

char NumeroParaLetra(int n)
{
    assert(n >= 1 && n <= 26);
    return "abcdefghijklmnopqrstuvwxyz"[n-1];
}
Perfil *Game::PerfilRandom() {
    stringstream ss;
    string letra;
    ss << NumeroParaLetra(_perfisTotais.size()+1);
    if(ss >> letra){
        Perfil *p = new Perfil(letra);
        int Ataque[2] ={5,6};
        int Defesa[6] ={12,13,14,2,3,4};
        int Movimento[3] ={9,10,11};
        int Alvo[2] = {7,8};
        p->AdicionarCaracteristica(CriarCaracteristica(1,p));
        do{
            p->AdicionarCaracteristica(CriarCaracteristica(Ataque[0 + rand() % 2],p));
            p->AdicionarCaracteristica(CriarCaracteristica(Defesa[0 + rand() % 6],p));
            p->AdicionarCaracteristica(CriarCaracteristica(Movimento[0 + rand() % 3],p));
            p->AdicionarCaracteristica(CriarCaracteristica(Alvo[0 + rand() % 2],p));
        }while(p->getMaxForca()>0);
        return p;
    }
    return nullptr;


}

Caracteristica *Game::CriarCaracteristica(int id, Perfil *perfil) {
    Caracteristica *c;
    int ataque=0, defesa=0, velocidade=0, vida =0;
    c= nullptr;
    switch (id)
    {
        case 1:
            c = new Bandeira();break;
        case 2:
            c = new Superior();
            vida++;
            break;
        case 3:
            c = new PeleDura();
            defesa++;
            break;
        case 4:
            c = new Armadura();
            defesa += 2;
            break;
        case 5:
            c = new Faca();
            ataque ++;
            break;
        case 6:
            c = new Espada();
            ataque += 3;
            break;
        case 7:
            c = new Agressao();break;
        case 8:
            c = new Ecologico();break;
        case 9:
            c = new HeatSeeker();
            velocidade++;
            break;
        case 10:
            c = new BuildSeeker();
            velocidade++;
            break;
        case 11:
            c = new Walker();
            velocidade++;
            break;
        case 12:
            c = new Remedio();break;
        case 13:
            c = new SecondChance();break;
        case 14:
            c = new Aluno();break;

        default:break;
    }
    if(c!= nullptr)
        perfil->AdicionarValoresCumulativos(ataque, defesa, velocidade, vida);
    else
        delete c;
    return c;
}

ExitType* Game::CheckEnd() {

    if(_exitType->_exitType==ExitType::NONE){
        if(_colonias[0]->Perdeu()){
            _exitType->_exitType=ExitType::LOSS;
            return _exitType;
        }
        bool OnlyOneAlive = true;
        for(int i =1;i<_colonias.size();i++){
            bool perdeu = _colonias[i]->Perdeu();
            if(!perdeu){
                OnlyOneAlive = false;
            }
        }
        if(OnlyOneAlive){
            _exitType->_exitType=ExitType::WIN;
            return _exitType;
        }
        if(_jogando==false){
            _exitType->_exitType=ExitType::FIM;
            return _exitType;
        }
    }
    return _exitType;
}

Game::Game(ExitType *exitType) {
    _cfg = new Configuracao(this);
    msg = new Mensagem();
    _exitType = exitType;
}

void Game::SairJogo(ExitType::type eType, string str) {
    _exitType->_exitType=eType;
    _exitType->_exitArgs=str;
}




